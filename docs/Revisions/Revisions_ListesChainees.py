class Maillon :
    def __init__(self, data = None, suivant = None)  :
        self.head = data
        self.tail = suivant

class Equipe :
    def __init__(self, first = None) :
        self.premier_maillon = first
    
    def est_vide(self) :
        return self.premier_maillon == None
    
    def tete(self) :
        return self.premier_maillon.head
    
    def queue(self) :
        if not (self.est_vide()):
            return Equipe(self.premier_maillon.tail)
    
    def ajout(self, data) :
        nouveau_maillon = Maillon(data, self.premier_maillon)
        self.premier_maillon = nouveau_maillon
 
france2003 = Equipe()
france2003.ajout(('Christine Arron', 10.41))
france2003.ajout(('Sylviane Félix', 10.48))
france2003.ajout(('Muriel Hurtis', 10.35))
france2003.ajout(('Patricia Girard', 10.54))
 
usa2003 = Equipe()
usa2003.ajout(('Torri Edwards', 10.38))
usa2003.ajout(('Inger Miller', 10.42))
usa2003.ajout(('Chrystel Gaines', 10.46))
usa2003.ajout(('Angela Williams', 10.57))

# question 1b


# question 1c


# question 2
def tab_athletes(equipe) :
    pass

print(tab_athletes(france2003))

# question 3

def temps_total(equipe) :
    if equipe.est_vide() :
        return ...
    else :
        return ... + temps_total(...)
    
print(temps_total(usa2003))

# question 4
def medailleOr(tab_equipe):
    tab_temps = [ temps_total(team) for team in tab_equipe]
    indice_mini = 0
    ...
    return tab_equipe[indice_mini]

vainqueur = medailleOr([france2003, usa2003])

# question 5
def mystere(equipe) :
    if equipe.queue().est_vide() :
        return True
    else :
        t1 = equipe.tete()[1]
        t2 = equipe.queue().tete()[1]
        return t1 > t2 and mystere(equipe.queue())
    
