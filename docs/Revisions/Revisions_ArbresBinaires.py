# Interface d'arbre binaire
class Noeud :
    def __init__(self, valeur, sag=None, sad=None) :
        self.val = valeur
        self.left = sag
        self.right= sad
        
def estvide(A) :
    return A is None

def racine(A) :
    return A.val

def gauche(A) :
    return A.left

def droit(A) :
    return A.right


# Partie A

# Voici un arbre de taille 10 et de hauteur 2
arb = Noeud('I')
arb.left = Noeud('M', None, Noeud('G', Noeud('A'), Noeud('N')) )
arb.right = Noeud('Q', Noeud('F', None, Noeud('I')), Noeud('E', Noeud('U'), None))


# Question 4.b
def parcours_mystere(A) :
    if not(estvide(A)) :
        parcours_mystere(gauche(A))
        parcours_mystere(droit(A))
        print(racine(A))

parcours_mystere(arb)

# Partie B

abr1 = Noeud(20)
abr1.left = Noeud(10, None, Noeud(17, Noeud(12), Noeud(19)) )
abr1.right = Noeud(50, Noeud(23, None, Noeud(40)), Noeud(100, Noeud(88), None))
