def recherche_dicho_recursif(tab, val, debut, fin):
    """
    Entrées : 
        tab est un tableau de nombres trié par ordre croissant 
        val est un nombre
        debut et fin sont des entiers tel que 0<= debut, fin < longeur(tab)
    Sortie : un booléen qui vaut True si val appartient au tableau et False sinon
    """
    if debut > fin : # cas de base
        return ... # compléter ici
    # on n'est pas dans le cas de base
    milieu = (debut + fin) // 2
    if val == tab[milieu] :
        return ... # compléter ici
    else :
        if val > tab[milieu] :
            return ... # compléter ici
        else :
            return ... # compléter ici
        
# quelques tests
monTab = [0, 1, 1, 2, 3, 6, 8, 12, 21]
val1 = 6
assert recherche_dicho_recursif(monTab, val1, 0, len(monTab)-1) == True, "problème de code 1"
val2 = 7
assert recherche_dicho_recursif(monTab, val2, 0, len(monTab)-1) == False, "problème de code 2"