import p5
from random import *
def est_devant_mur(laby,avatar):
    """
    renvoie
        True si l'avatar est devant un mur,
        False s'il y a une case devant
    """
    voisins=laby[(avatar.x,avatar.y)]
    if avatar.direction=="est":
        if (avatar.x+1,avatar.y) in voisins:
            return False
        else:
            return True
    elif avatar.direction=="ouest":
        if (avatar.x-1,avatar.y) in voisins:
            return False
        else:
            return True
    elif avatar.direction=="nord":
        if (avatar.x,avatar.y-1) in voisins:
            return False
        else:
            return True
    elif avatar.direction=="sud":
        if (avatar.x,avatar.y+1) in voisins:
            return False
        else:
            return True
        
def case_devant(laby,avatar):
    """
    retourne la case devant l'avatar
    """
    if est_devant_mur(laby,avatar)==False:
        if avatar.direction=="est":
            return (avatar.x+1,avatar.y)
        elif avatar.direction=="ouest":
            return (avatar.x-1,avatar.y)
        elif avatar.direction=="nord":
            return (avatar.x,avatar.y-1)
        elif avatar.direction=="sud":
            return (avatar.x,avatar.y+1)
    else:
        raise IndexError('avatar est devant un mur, pas de case')

def est_ouvert_droit(laby,avatar):
    """
    retourne
        True s'il y a une case dispo à droite de l'avatar
        False sinon
    """
    voisins=laby[(avatar.x,avatar.y)]
    if avatar.direction=="est":
        if (avatar.x,avatar.y+1) in voisins:
            return True
        else:
            return False
    elif avatar.direction=="ouest":
        if (avatar.x,avatar.y-1) in voisins:
            return True
        else:
            return False
    elif avatar.direction=="nord":
        if (avatar.x+1,avatar.y) in voisins:
            return True
        else:
            return False
    elif avatar.direction=="sud":
        if (avatar.x-1,avatar.y) in voisins:
            return True
        else:
            return False

def est_ouvert_gauche(laby,avatar):
    """
    retourne
        True s'il y a une case dispo à gauche de l'avatar
        False sinon
    """
    voisins=laby[(avatar.x,avatar.y)]
    if avatar.direction=="est":
        if (avatar.x,avatar.y-1) in voisins:
            return True
        else:
            return False
    elif avatar.direction=="ouest":
        if (avatar.x,avatar.y+1) in voisins:
            return True
        else:
            return False
    elif avatar.direction=="nord":
        if (avatar.x-1,avatar.y) in voisins:
            return True
        else:
            return False
    elif avatar.direction=="sud":
        if (avatar.x+1,avatar.y) in voisins:
            return True
        else:
            return False
        
def case_a_droite(laby,avatar):
    """
    retourne les coordonnées de la case à droite
    """
    voisins=laby[(avatar.x,avatar.y)]
    if avatar.direction=="est":
        return (avatar.x,avatar.y+1)
    elif avatar.direction=="ouest":
        return (avatar.x,avatar.y-1)
    elif avatar.direction=="nord":
        return (avatar.x+1,avatar.y)
    elif avatar.direction=="sud":
        return (avatar.x-1,avatar.y)
    
def case_a_gauche(laby,avatar):
    """
    retourne les coordonnées du sommet à gauche
    """
    voisins=laby[(avatar.x,avatar.y)]
    if avatar.direction=="est":
        return (avatar.x,avatar.y-1)
    elif avatar.direction=="ouest":
        return  (avatar.x,avatar.y+1)
    elif avatar.direction=="nord":
        return (avatar.x-1,avatar.y)
    elif avatar.direction=="sud":
        return (avatar.x+1,avatar.y)
    
class Personnage:
    """
    Personnage
    """
    def __init__(self,x=0,y=0,direction="est"):
        self.x=x # abscisse 
        self.y=y # ordonnée
        self.direction=direction # direction
        
class Laby3d:
    # Constantes
    BLANC=(255,255,255)
    NOIR=(0,0,0)
    VERT=(0,255,0)
    VERT_CLAIR=(148,255,0)
    ROUGE=(255,0,0)
    MARRON=(165, 42, 42)
    BEIGE=(255,251,179)
    BLEU_CLAIR=(2,140,253)
    BLEU=(0,0,255)
    JAUNE=(240,217,105)
    def fleche_droite(self):
        x1=p5.mouseX
        y1=p5.mouseY
        x2=p5.mouseX-self.largeur_fenetre/20
        y2=p5.mouseY-self.hauteur_fenetre/20
        x3=p5.mouseX-self.largeur_fenetre/20
        y3=p5.mouseY+self.hauteur_fenetre/20
        p5.fill((255,255,255,100))
        p5.triangle(x1,y1,x2,y2,x3,y3)
        x1=p5.mouseX-self.largeur_fenetre/20
        y1=p5.mouseY-self.largeur_fenetre/60
        p5.rect(x1,y1,-self.largeur_fenetre/20,self.largeur_fenetre/30)
    def fleche_gauche(self):
        x1=p5.mouseX
        y1=p5.mouseY
        x2=p5.mouseX+self.largeur_fenetre/20
        y2=p5.mouseY-self.hauteur_fenetre/20
        x3=p5.mouseX+self.largeur_fenetre/20
        y3=p5.mouseY+self.hauteur_fenetre/20
        p5.fill((255,255,255,100))
        p5.triangle(x1,y1,x2,y2,x3,y3)
        x1=p5.mouseX+self.largeur_fenetre/20
        y1=p5.mouseY-self.largeur_fenetre/60
        p5.rect(x1,y1,self.largeur_fenetre/20,self.largeur_fenetre/30)
    def fleche_avant(self):
        x1=p5.mouseX
        y1=p5.mouseY   
        x2=p5.mouseX+self.largeur_fenetre/20
        y2=p5.mouseY+self.largeur_fenetre/60
        x3=p5.mouseX-self.largeur_fenetre/20
        y3=p5.mouseY+self.largeur_fenetre/60
        p5.fill((255,255,255,100))
        p5.triangle(x1,y1,x2,y2,x3,y3)
        x1=p5.mouseX+self.largeur_fenetre/60
        y1=p5.mouseY+self.largeur_fenetre/60
        x2=p5.mouseX-self.largeur_fenetre/60
        y2=p5.mouseY+self.largeur_fenetre/60
        x3=p5.mouseX+self.largeur_fenetre/40
        y3=p5.mouseY+self.largeur_fenetre/30
        x4=p5.mouseX-self.largeur_fenetre/40
        y4=p5.mouseY+self.largeur_fenetre/30
        p5.quad(x1,y1,x2,y2,x4,y4,x3,y3)
        
    def update(self):
        # emulation d'un mouse released
        if p5.mouseIsPressed:
            self.mousePressed=True
        else:
            if self.mousePressed==True and p5.mouseY>0 and p5.mouseY<self.largeur_fenetre:
                # souris relachée
                self.mousePressed=False
                # gestion du bouton
                if p5.mouseX<self.largeur_fenetre and p5.mouseX>self.largeur_fenetre-15 and p5.mouseY<15 and p5.mouseY>0:
                    if self.switch_laby2d==True:
                        self.switch_laby2d=False
                    else:
                        self.switch_laby2d=True
                else: # gestion deplacement
                    if p5.mouseX<self.largeur_fenetre/4:
                        # tourner à gauche
                        cycle=["nord","ouest","sud","est"]
                        if self.personnage.direction==cycle[-1]:
                            self.personnage.direction=cycle[0]
                        else:
                            self.personnage.direction=cycle[cycle.index(self.personnage.direction)+1]
                    elif p5.mouseX>self.largeur_fenetre*3/4:
                        # tourner à droite
                        cycle=["nord","est","sud","ouest"]
                        if self.personnage.direction==cycle[-1]:
                            self.personnage.direction=cycle[0]
                        else:
                            self.personnage.direction=cycle[cycle.index(self.personnage.direction)+1]
                    else:
                        # avancer
                        if est_devant_mur(self.laby,self.personnage)==False:
                            new_case=case_devant(self.laby,self.personnage)
                            self.personnage.x=new_case[0]
                            self.personnage.y=new_case[1]
                
    def dessinerBord(self,x,y,direction):
            if direction=="nord":
                ma_ligne=p5.line(x*self.taille_case,y*self.taille_case,x*self.taille_case+self.taille_case,y*self.taille_case)
            elif direction=="sud":
                 ma_ligne=p5.line(x*self.taille_case,y*self.taille_case+self.taille_case,x*self.taille_case+self.taille_case,y*self.taille_case+self.taille_case)
            elif direction=="est":
                 ma_ligne=p5.line(x*self.taille_case+self.taille_case,y*self.taille_case,x*self.taille_case+self.taille_case,y*self.taille_case+self.taille_case)
            elif direction=="ouest":
                 ma_ligne=p5.line(x*self.taille_case,y*self.taille_case,x*self.taille_case,y*self.taille_case+self.taille_case)

    def dessin_laby2d(self,coordonnees=True):
        self.coordonnees=coordonnees
        self.taille_case=self.largeur_fenetre/(5*self.largeur)
        # tracé du fond
        alpha=200
        c=p5.color(255, 255, 255, alpha)
        p5.fill(c)
        p5.rect(0, 0, self.taille_case*self.largeur, self.taille_case*self.hauteur)
        for sommet in self.laby:
            x=sommet[0]
            y=sommet[1]
            destinations=self.laby[sommet]
            nord=False
            sud=False
            est=False
            ouest=False
            for destination in destinations:
                x_destination=destination[0]
                y_destination=destination[1]
                if x==x_destination:
                    if y+1==y_destination:
                        sud=True
                    elif y-1==y_destination:
                        nord=True
                elif y==y_destination:
                    if x-1==x_destination:
                        ouest=True
                    elif x+1==x_destination:
                        est=True

            # tracé des murs
            v=(0,255,0,alpha)
            p5.stroke(v)
            if nord==False:
                self.dessinerBord(x,y,"nord")
            if sud==False:
                self.dessinerBord(x,y,"sud")
            if est==False:
                self.dessinerBord(x,y,"est")
            if ouest==False:
                self.dessinerBord(x,y,"ouest")
            # tracé des coordonnées
            if self.coordonnees==True:
                p5.noStroke()
                p5.fill("black")
                texte="("+str(x)+","+str(y)+")"
                p5.textSize(self.taille_case/4)
                p5.text(texte,x*self.taille_case+self.taille_case/5,y*self.taille_case+self.taille_case/1.8)
        # tracé sommet_depart
        p5.fill((165, 42, 42,alpha))
        p5.circle(self.sommet_depart[0]*self.taille_case+self.taille_case/2,self.sommet_depart[1]*self.taille_case+self.taille_case/2,self.taille_case*0.8)
        # tracé sommet_destination
        p5.fill((0,255,0,alpha))
        p5.circle(self.sommet_destination[0]*self.taille_case+self.taille_case/2,self.sommet_destination[1]*self.taille_case+self.taille_case/2,self.taille_case*0.8)
        # tracé du perso
        r=(255,0,0,alpha)
        p5.fill(r)
        p5.circle(self.personnage.x*self.taille_case+self.taille_case/2,self.personnage.y*self.taille_case+self.taille_case/2,self.taille_case/2*0.8)
        if self.personnage.direction=="est" or self.personnage.direction=="ouest":
            x1=self.personnage.x*self.taille_case+self.taille_case/2
            x2=self.personnage.x*self.taille_case+self.taille_case/2
            y1=self.personnage.y*self.taille_case+self.taille_case/2-self.taille_case*0.12
            y2=self.personnage.y*self.taille_case+self.taille_case/2+self.taille_case*0.12
            if self.personnage.direction=="est":
                x3=self.personnage.x*self.taille_case+self.taille_case
                y3=self.personnage.y*self.taille_case+self.taille_case/2
            else: # ouest
                x3=self.personnage.x*self.taille_case
                y3=self.personnage.y*self.taille_case+self.taille_case/2
        else: #nord et sud
            x1=self.personnage.x*self.taille_case+self.taille_case/2-self.taille_case*0.12
            y1=self.personnage.y*self.taille_case+self.taille_case/2
            x2=self.personnage.x*self.taille_case+self.taille_case/2+self.taille_case*0.12
            y2=self.personnage.y*self.taille_case+self.taille_case/2
            if self.personnage.direction=="nord":
                x3=self.personnage.x*self.taille_case+self.taille_case/2
                y3=self.personnage.y*self.taille_case
            else: # sud
                x3=self.personnage.x*self.taille_case+self.taille_case/2
                y3=self.personnage.y*self.taille_case+self.taille_case 
        p5.triangle(x1,y1,x2,y2,x3,y3)
        
            
            
    # init
    def __init__(self,laby,largeur,hauteur,personnage=Personnage(0,0,"est")):
        self.laby=laby
        self.largeur=largeur
        self.hauteur=hauteur
        self.largeur_fenetre=800
        self.hauteur_fenetre=600
        self.personnage=personnage
        self.sommet_depart=(personnage.x,personnage.y)
        self.sommet_destination=(self.largeur-1,randint(0,self.hauteur-1))
        self.mousePressed=False
        self.switch_laby2d=False # afficher ou pas le laby en 2d
        self.run()

    def setup(self):
        p5.createCanvas(self.largeur_fenetre,self.hauteur_fenetre)
        p5.background("white")
        p5.noCursor()
        
    def draw(self):
        # Mise à jour de la fenêtre
        self.update()
        # dessin de la fenêtre
        p5.fill("black")
        p5.rect(0, 0, self.largeur_fenetre, self.hauteur_fenetre)
        p5.stroke("white")
        #construction du premier plan
        # On définit un avatar modifiable du personnage
        avatar=Personnage(self.personnage.x,self.personnage.y,self.personnage.direction)
        # On définit le premier cadre exterier sur la toatlité de la fenêtre
        cadre_exterieur=(0,0,self.largeur_fenetre,self.hauteur_fenetre)
        # On calcule le cadre interieur de la première profondeur
        x_interieur=cadre_exterieur[0]+(cadre_exterieur[2]-cadre_exterieur[0])//6
        y_interieur=cadre_exterieur[1]+(cadre_exterieur[3]-cadre_exterieur[1])//6
        largeur_cadre_interieur=cadre_exterieur[2]*2//3
        hauteur_cadre_interieur=cadre_exterieur[3]*2//3
        cadre_interieur=(x_interieur,y_interieur,largeur_cadre_interieur,hauteur_cadre_interieur)
        # Un drapeau pour savoir si on peut avancer en profondeur dans le dessin
        fin_couloir=False
        while fin_couloir==False:
            if self.personnage.direction=="ouest" or self.personnage.direction=="est":
                couleur_cote=Laby3d.BLEU
                couleur_face=Laby3d.BLEU_CLAIR
            else:
                couleur_cote=Laby3d.BLEU_CLAIR
                couleur_face=Laby3d.BLEU
            if est_ouvert_gauche(self.laby,avatar)==True:
                # s'il y a une sortie à gauche
                p5.fill(couleur_cote)
                p5.rect(cadre_exterieur[0],cadre_interieur[1],cadre_interieur[0]-cadre_exterieur[0],cadre_interieur[3])
            else:
                # sinon il y a un mur à gauche
                p5.fill(couleur_face)
                p5.quad(cadre_exterieur[0],cadre_exterieur[1],cadre_exterieur[0],cadre_exterieur[1]+cadre_exterieur[3],cadre_interieur[0],cadre_interieur[1]+cadre_interieur[3],cadre_interieur[0],cadre_interieur[1])

            if est_ouvert_droit(self.laby,avatar)==True:
                # si il y a une sortie à droite
                p5.fill(couleur_cote)
                p5.rect(cadre_interieur[0]+cadre_interieur[2],cadre_interieur[1],(cadre_exterieur[2]-cadre_interieur[2])//2,cadre_interieur[3])
            else:
                # sinon il y a un mur à droite
                p5.fill(couleur_face)
                p5.quad(cadre_exterieur[0]+cadre_exterieur[2],cadre_exterieur[1],cadre_interieur[0]+cadre_interieur[2],cadre_interieur[1],cadre_interieur[0]+cadre_interieur[2],cadre_interieur[1]+cadre_interieur[3],cadre_exterieur[0]+cadre_exterieur[2],cadre_exterieur[1]+cadre_exterieur[3])
            # dessin sol
            if avatar.x==self.sommet_depart[0] and avatar.y==self.sommet_depart[1]:
                # si je suis sur le sommet depart
                p5.fill(Laby3d.MARRON)
                p5.quad(cadre_interieur[0],cadre_interieur[1]+cadre_interieur[3],cadre_exterieur[0],cadre_exterieur[1]+cadre_exterieur[3],cadre_exterieur[0]+cadre_exterieur[2],cadre_exterieur[1]+cadre_exterieur[3],cadre_interieur[0]+cadre_interieur[2],cadre_interieur[1]+cadre_interieur[3])
            # dessin case destination
            if avatar.x==self.sommet_destination[0] and avatar.y==self.sommet_destination[1]:
                # si je suis sur le sommet destination
                p5.fill((randint(0,255),randint(0,255),randint(0,255)))
                p5.quad(cadre_interieur[0],cadre_interieur[1]+cadre_interieur[3],cadre_exterieur[0],cadre_exterieur[1]+cadre_exterieur[3],cadre_exterieur[0]+cadre_exterieur[2],cadre_exterieur[1]+cadre_exterieur[3],cadre_interieur[0]+cadre_interieur[2],cadre_interieur[1]+cadre_interieur[3])
            # y a t il un mur devant moi
            if est_devant_mur(self.laby,avatar)==True:
                # si devant moi il y a un mur
                p5.fill(couleur_cote)
                p5.rect(cadre_interieur[0],cadre_interieur[1],cadre_interieur[2],cadre_interieur[3])
                fin_couloir=True
            else:
                # sinon, il y a une case devant moi
                # On cherche les coordonnées de la case
                new_case=case_devant(self.laby,avatar)
                # On met l'avatar dessus
                avatar.x=new_case[0]
                avatar.y=new_case[1]
                # Le cadre interieur devient le cadre exterieur
                cadre_exterieur=cadre_interieur
                # On calcule les coordonnées et dimensions du nouveau cadre intérieur
                new_x_interieur=cadre_exterieur[0]+cadre_exterieur[2]//6
                new_y_interieur=cadre_exterieur[1]+cadre_exterieur[3]//6
                new_largeur_cadre_interieur=cadre_exterieur[2]*2//3
                new_hauteur_cadre_interieur=cadre_exterieur[3]*2//3
                cadre_interieur=(new_x_interieur,new_y_interieur,new_largeur_cadre_interieur,new_hauteur_cadre_interieur)
                # On repart pour un tour pour dessiner la profondeur suivante
   
        if self.switch_laby2d==True:
            # on dessine le laby en 2D
            self.dessin_laby2d()
        # dessin du bouton pour laby2d
        p5.fill(Laby3d.MARRON)
        p5.rect(self.largeur_fenetre-15,0,15,15)
        p5.noStroke()
        p5.fill("white")
        texte="L"
        p5.textSize(15)
        p5.text(texte,self.largeur_fenetre-12,13)
        # dessin fleche
        if p5.mouseX<self.largeur_fenetre and p5.mouseX>self.largeur_fenetre-15 and p5.mouseY<15 and p5.mouseY>0:
            p5.cursor()
        else:
            p5.noCursor()
            if p5.mouseX<self.largeur_fenetre/6:
                self.fleche_gauche()
            elif p5.mouseX>self.largeur_fenetre*5/6:
                self.fleche_droite()
            else:
                self.fleche_avant()
                          
    def run(self):
        p5.run(self.setup, self.draw)


