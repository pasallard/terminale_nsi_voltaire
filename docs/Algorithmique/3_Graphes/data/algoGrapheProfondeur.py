#--- HDR ---#
# pile
def creer_pile_vide()  :
    return []

def empiler(p, donnée) :
    p.append(donnée) # on ajoute la donnée à la fin du tableau
    return p # en fait, c'est superflu : en Python, la liste "pile" est modifiée en place 

def depiler(p) :
    aEnlever = p.pop( len(p) - 1 ) # la donnée à enlever est positionnée à la fin du tableau
    return aEnlever

def est_vide(p)  :
    return len(p) == 0 # renvoie le booléen True si la longueur est nulle et False sinon

# interface d'un graphe non orienté
def creer_graphe_vide():
    return dict()

def liste_sommets(G):
    return [s for s in G.keys()]

def liste_voisins(G,s) :
    return G[s]

def sont_voisins(G,s1,s2) :
    return s2 in G[s1]

def ajouter_sommet(G,s) :
    assert s not in G.keys(), "Ce sommet existe déja"
    G[s] = []
    return None 

def ajouter_arete(G, s1, s2) :
    if not sont_voisins(G, s1, s2) :
        G[s1].append(s2)
        G[s2].append(s1)
    return None 
#--- HDR ---#
def parcours_profondeur(G,s):
    p = creer_pile_vide()
    empiler(p,s)
    tab_decouverts = [s]
    while not est_vide(p):
        s = depiler(p)
        print(s, end=" - ")
        for v in liste_voisins(G,s) :
            if v not in tab_decouverts :
                empiler(p,v)
                tab_decouverts.append(v)
    return None

# modélisation du graphe par un dictionnaire
graphe = {"B" : ["A", "D", "E"], "A" : ["B", "C"], "C" : ["A", "D"], "D":["B", "C", "E"],
"E" : ["B", "F", "G"], "F" : ["E", "G"], "G" : ["E", "F", "H"], "H":["G"]}
# parcours du graphe
parcours_profondeur(graphe, "B")