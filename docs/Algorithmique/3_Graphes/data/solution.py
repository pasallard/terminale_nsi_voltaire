# Créé par scsc, le 22/04/2021 en Python 3.7
def solution1(laby):
    laby_correct={(0, 0): [(0,1)],
      (0, 1): [(0, 2), (0, 0)],
      (0, 2): [(1, 2), (0, 1), (0, 3)],
      (0, 3): [(0, 2)],
      (1, 0): [(1, 1)],
      (1, 1): [(1, 0), (2, 1)],
      (1, 2): [(1, 3), (0, 2)],
      (1, 3): [(2, 3), (1, 2)],
      (2, 0): [(2, 1), (3, 0)],
      (2, 1): [(1, 1), (2, 0)],
      (2, 2): [(3, 2), (2, 3)],
      (2, 3): [(2, 2), (1, 3), (3, 3)],
      (3, 0): [(2, 0), (3, 1)],
      (3, 1): [(3, 0), (3, 2)],
      (3, 2): [(3, 1), (2, 2)],
      (3, 3): [(2, 3)]}
    drapeau=True
    for s in laby:
        if s in laby_correct:
            liste=laby[s]
            liste_correct=laby_correct[s]
            for l in liste:
                if l not in liste_correct:
                    drapeau = False
            for l in liste_correct:
                if l not in liste:
                    drapeau = False
        else:
            drapeau=False
    for s in laby_correct:
        if s in laby:
            liste=laby[s]
            liste_correct=laby_correct[s]
            for l in liste:
                if l not in liste_correct:
                    drapeau = False
            for l in liste_correct:
                if l not in liste:
                    drapeau = False
        else:
            drapeau=False
    if drapeau==True:
        return "Bonne réponse"
    else:
        return "Mauvaise réponse"
    
def solution2():
    a=input("Quelle est votre réponse (A, B ou C) : ")
    if a=="B":
        print("Bonne réponse")
    else:
        print("Mauvaise réponse")
        
