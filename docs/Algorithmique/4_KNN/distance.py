from math import sqrt # sqrt = SQuare RooT
def distance3D(P,Q) :
    """P et Q sont des tuples formés de 3 coordonnées"""
    (xP, yP, zP) = P
    (xQ, yQ, zQ) = Q
    PQ_au_carré = (xQ-xP)**2+(yQ-yP)**2+(zQ-zP)**2
    return sqrt(PQ_au_carré)