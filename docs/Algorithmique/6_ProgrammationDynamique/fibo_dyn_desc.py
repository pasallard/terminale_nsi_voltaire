def F(n):
    """ programmation dynamique, version descendante"""
    tab_memo = [0,1] + [None] * (n-1) # les deux premières valeurs, suivies de None
    return Fibo_memo(n, tab_memo)

def Fibo_memo(k, tab_memo) :
    if tab_memo[k] is None :
         # print("étape ", k) # pour visualiser les appels récursifs
        tab_memo[k] =  Fibo_memo(k-1, tab_memo) +  Fibo_memo(k-2, tab_memo) # appels récursifs
    return tab_memo[k]