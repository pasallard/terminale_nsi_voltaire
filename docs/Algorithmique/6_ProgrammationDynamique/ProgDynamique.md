# Programmation dynamique :dancer:

## ^^1. Introduction^^

!!! note "Catégorisation de certains principes algorithmiques"

    * **Principe glouton** : construit une solution en optimisant un critère de manière locale, sans garantie d'obtenir la meilleure solution possible. Exemple : problème du voyageur de commerce, problème du rendu de monnaie.

    * **Diviser pour régner** : divise un problème en sous-problèmes indépendants (qui ne se chevauchent pas), résout chaque sous-problème, et combine les solutions des sous-problèmes pour former  une solution du problème initial. Exemple : recherche par dichotomie dans un tableau trié, algorithme de tri-fusion.

    * 👉 **Programmation dynamique** : divise un problème en sous-problèmes qui ne sont pas indépendants (qui se chevauchent), et cherche (et stocke) des solutions aux sous-problèmes pour aboutir à une solution du problème initial.


!!! warning ""
    Il est important de noter que "programmation" dans "programmation dynamique" ne doit pas s'entendre comme "utilisation d'un langage de programmation", mais comme synonyme de planification et ordonnancement.


## ^^2. Exemple fondateur : le calcul des nombres de Fibonacci^^

Fibonacci (1170-1250) est un mathématicien italien, qui a étudié cette suite de nombres : 

**0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...**

Voyez-vous la logique de construction de cette suite de nombres ? Quel nombre vient après 55 ? et ensuite ?

??? "Solution"
    On additionne deux nombres qui se suivent pour obtenir le nombre suivant.

    ![fibo1](fibo_1.png) 

    Ainsi, après 34 et 55, il vient 89. Et ensuite 55 + 89 = 144.


Mathématiquement, on donne une définition récursive de la suite `F(n)` des nombres de Fibonacci : 

    Calcul de F(n) :
        Si n vaut 0 ou 1 :
            F(n) = n
        Sinon
            F(n) = F(n-1) + F(n-2)

### 2.1. Un algorithme de calcul particulièrement inefficace


Pour calculer le nombre `F(n)`, on peut utiliser cette définition récursive (_par récurrence_ diront les mathématiciens) pour concevoir un programme récursif.


{{ IDEv('fibo_rec') }}

!!! question "Version récursive "

    Tester dans la console `F(11)`, puis `F(40)`. Que constatez-vous ?


??? danger "Solution"

    F(40) est très long à calculer, alors que ce n'est que le 41^e^ terme de la suite.

!!! note "Les appels récursifs"

    Détaillons les étapes du calcul de `F(7)` :
    
    - pour calculer `F(7)`, il faut calculer `F(6)` et `F(5)` ;
    - et pour calculer `F(6)`, il faut calculer `F(5)` et `F(4)` ;
    - et pour calculer `F(5)`, il faut calculer `F(4)` et `F(3)` ; etc.  


    On peut représenter cela avec l'arbre des appels récursifs:

    ![tab_recur](prog_dyn_recur.png){: .center}

    Source : [Algorithms, J. Erickson](http://jeffe.cs.illinois.edu/teaching/algorithms/)

    On remarque que le calcul de `F(5)` est lancé deux fois (une fois pour `F(6)` et une fois pour `F(7)`), que celui de `F(4)` est lancé 3 fois et que celui de `F(3)` est calculé 5 fois. 

    Plus on augmente le numéro de rang, plus il y a des calculs qui sont répétés et refaits. 
    
    La complexité de cet algorithme est exponentielle :scream: !

### 2.2. Calcul par programmation dynamique

!!! note "Principe de calcul par programmation dynamique"
    Pour éviter de refaire plusieurs fois le même calcul, on va utiliser un tableau pour stocker les valeurs des nombres `F(n)` déjà calculés.

    Les informaticiens parlent de ***mémo(r)ïsation***.

#### 2.2.1. Version ascendante, en allant de `F(0)` jusqu'à `F(n)`

L'idée toute simple pour calculer `F(n)` est de créer un tableau de taille `n+1` rempli initialement de 0 et de le remplir grâce à une boucle `for` à l'aide de la formule `F(n) = F(n-1) + F(n-2)`.

{{ IDEv('fibo_dyn_asc') }}

!!! question "Programmation dynamique ascendante"

    Compléter le code, puis tester dans la console `F(11)` et `F(40)`.
    
    Que constatez-vous ?

#### 2.2.2. Version descendante, en partant de `F(n)` et en descendant jusqu'à `F(0)`

Ici, l'idée est encore d'utiliser un tableau, mais avec seulement ses deux premières cases remplies : les suivantes sont laissées "vides" grâce à des `None`.

On utilise ensuite une fonction auxiliaire ***récursive*** pour remplacer les `None` par des nombres.

{{ IDE('fibo_dyn_desc') }}

!!! question "Programmation dynamique descendante"

    Analyser le code puis tester dans la console `F(11)` et `F(40)`.

    Décommenter la ligne du `print(k)` pour visualiser les appels récursifs.
    

??? danger "Explications"

    Voici l'arbre des appels récursifs du calcul de `F(7)` en programmation dynamique descendante. Les flèches vertes descendantes indiquent l'écriture dans le tableau de ***mémo(r)ïsation***, tandis que les flèches rouges montantes indiquent la lecture dans ce tableau.

    ![tab_memo](prog_dyn_desc.png){: .center}

    Source : [Algorithms, J. Erickson](http://jeffe.cs.illinois.edu/teaching/algorithms/)

#### 2.3. Complexité en temps 

!!! question "Complexité algorithmique"

    Quelle est la complexité (en temps) des algorithmes de calcul de `F(n)` par programmation dynamique, que ce soit en version ascendante ou descendante ? 

    Est-elle meilleure que la complexité exponentielle de la version récursive naïve ?

??? danger "Solution"
    La complexité en temps est linéaire : par exemple dans la version ascendante, il y a une seule boucle `for`.
    
    C'est bien meilleur qu'une complexité exponentielle.

## ^^3. Exercices type Bac^^


* Exercice type Bac n°1 : 

    * [énoncé](Prog_Recursivite_SujetBac_21Sujet0.pdf), à faire d'abord sur papier ;
    * [codage sur un carnet Jupyter](Prog_Recursivite_SujetBac_21Sujet0.ipynb), pour tester vos réponses et visualiser le résultat.


* Exercice type Bac n°2 : 

    * [énoncé](Prog_Dynamique_SujetBac_2024_Sujet0B.pdf), à faire sur papier.



