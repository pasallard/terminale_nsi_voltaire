# Algorithmes relatifs aux ABR (arbres binaires de recherche) :dart:

Commençons par un rappel (voir le cours dans le chapitre Structures de données):

!!! tip "Rappel : définition d'un ABR"
    Un **arbre binaire de recherche** (ABR) est :
    
    * un arbre binaire (tous ses nœuds ont ***au maximum deux fils*** : un fils gauche et un fils droit),
    * dans lequel la valeur de n'importe quel nœud vérifie les deux conditions suivantes :

        - la valeur du nœud est **supérieure** à _toutes_ les valeurs de son **fils gauche**
        - la valeur du nœud est **inférieure** à _toutes_ les valeurs de son **fils droit**

La mention "**de recherche**" provient du fait que la recherche d'une valeur dans un ABR est performante (_du moins en théorie_), ce que nous allons commencer par examiner.

## ^^1. Algorithme de recherche d'une valeur dans un ABR^^

### 1.1. Principe de l'algorithme

L'objectif est d'élaborer un algorithme qui, étant donnés un ABR et une valeur, renvoie `True` si la valeur appartient à l'ABR et `False` sinon.


!!! tip "Recherche dans un ABR :heart:"
    
    Classiquement avec les structures d'arbres binaires, on peut poser un algorithme récursif :

        FONCTION Contient_valeur(abr, val)

            SI est_vide(abr) ALORS
                RENVOYER False # cas de base 
            SINON
                SI valeur(abr) = val ALORS 
                    RENVOYER True # la valeur est trouvée, on sort de la fonction
                FIN_SI
                SI val < valeur(abr) ALORS
                    RENVOYER Contient_valeur(sous_arbre_gauche(abr), val) # on part du côté gauche
                SINON
                    RENVOYER Contient_valeur(sous_arbre_droit(abr), val) # on part du côté droit
            FIN_SI


Coder cet algorithme en Python en utilisant l'implémentation Orientée Objet d'un arbre.

{{ IDE("data/rechercheABRPOO")}}

Le test proposé concerne l'ABR suivant :
```mermaid
graph TD  
    A(25) --- B(19)
    A --- C(37)
    B --- D(7)
    B --- E(24)
    C --- F(29)
    C --- G(52)
```

??? danger "Solution"
    Un code possible est le suivant :

    ```python
    def  Contient_valeur(abr, val) :
    """ renvoie True si val est dans l'ABR et False sinon"""
        if abr is None :
            return False
        else :
            if abr.valeur == val :
                return True
            if val < abr.valeur :
                return Contient_valeur(abr.left, val)
            else :
                return Contient_valeur(abr.right, val)
    ```


### 1.2. Complexité algorithmique de la recherche dans un ABR équilibré

On appelle **arbre équilibré** un arbre dont les sous-arbres gauche et droit ont la même taille, comme par exemple l'arbre ci-dessous :

![](data/abr_search.png)

L'algorithme de recherche élaboré ci-dessus applique le principe ***"diviser pour régner"*** : à chaque étape (si la valeur n'a pas encore été trouvée), la charge de travail est **divisée par deux**.

Inversement, si on multiplie par 2 la taille de l'ABR, il n'y aura besoin que d'une étape supplémentaire pour y effectuer la recherche d'une valeur. Puisqu'il ne faut qu'une étape pour faire une recherche dans un ABR de taille 2 (1 feuille à droite, une feuille à gauche, on omet volontairement la racine pour simplifier les calculs), on peut dresser le tableau suivant :

| Taille $n$ de l'ABR équilibré (sans compter la racine)                   | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
|---------------------------------------------------|---|---|---|----|----|----|-----|-----|
| Nombre maximal $M$ d'appels récursifs  | 1 | 2 | 3 | 4  | 5  | 6  | 7   | 8   |

On voit ainsi apparaitre le lien mathématique entre ces deux quantités : $n = 2^M$ (la taille $n$ de l'ABR est égal à 2 à la puissance le nombre $M$ d'étapes de calcul).

Or ce que l'on cherche à connaitre, c'est la relation "inverse", à savoir le nombre d'étapes $M$ en fonction de la taille $n$. 
En mathématiques, on parle de "fonction réciproque" et la fonction réciproque de la fonction ***"2 à la puissance n***" s'appelle la fonction ***logarithme de base 2***.

Elle se note $\log_2$ (en mathématiques) et `log2` en Python (avec le module `math`) : on a donc $\log_2(8) = 3$, $\log_2(16) = 4$, etc. 

{{ IDE("data/calcul_log2", MAX=2)}}

Le nombre maximal d'appels récursifs $M$ de l'algorithme de recherche s'écrit alors $M = \log_2(n)$ : on dit que l'algorithme de recherche dans un ABR équilibré est de complexité **logarithmique**. 

Ceci le rend très performant : par exemple, pour un arbre contenant 1000 valeurs, 10 étapes suffisent car $\log_2(1000) \approx 9,96$.


> Remarque 1: en classe de Première, vous avez vu l'algorithme de [recherche par dichotomie](https://pasallard.gitlab.io/premiere_nsi_voltaire/Algorithmique/pageDeGarde/){target="_blank"} dans un tableau trié. Cet algorithme appliquait lui-aussi le principe de "diviser pour régner" et avait également une complexité logarithmique. Il y a donc une forte analogie entre les deux algorithmes, qui provient du fait qu'ils s'appliquent sur à des objets "bien rangés" (tableau trié par ordre croissant pour l'un, arbre binaire ***de recherche*** pour l'autre).

> Remarque 2 : la fonction réciproque de la fonction "élever à la puissance 2" est donc la fonction "logarithme de base 2". On retrouve un lien similaire entre deux fonctions plus classiques : la fonction "racine carrée" $g : x\mapsto \sqrt{x}$ est la fonction réciproque de la fonction "carré" $f: x\mapsto x^2$.

## ^^2. Insertion d'une valeur dans un ABR^^


### 2.1. Algorithme non récursif d'insertion

Quand vous avez pratiqué manuellement l'insertion d'une valeur dans un ABR, vous avez noté que cela consiste à ajouter une feuille au bon endroit.

Voici un algorithme (non récursif) d'insertion qui va reproduire nos opérations manuelles :

- on initalise un booléen `bonEndroit` à False qui sert à faire tourner une bouche `while` ainsi qu'un nœud temporaire `noeud_temp` qui matérialise notre position dans l'arbre (initialement à la racine) ;
- grâce à la boucle `while`, on descend dans l'arbre jusqu'à ce qu'on trouve la bonne place, c'est-à-dire quand il y a une _feuille vide_ au bon endroit ;
- quand on a trouvé cette feuille vide, on la remplace par la feuille contenant la valeur à insérer ; et le booléen `bonEndroit` passe à `True` pour mettre fin à la boucle `while`.

Un code possible en Python est :

{{ IDE("data/insertionABR_nonRecur")}}


_Remarque_ : le test proposé s'applique à l'ABR ci-dessous.
```mermaid
graph TD  
    A(25) --- B(19)
    A --- C(37)
    B --- D(7)
    B --- E(24)
    C --- F(29)
    C --- G(52)
```

### 2.2. Algorithme récursif d'insertion

Le code précédent (non récursif), qui reproduit les opérations que l'on fait manuellement pour ajouter une valeur dans un ABR, a le désavantage d'être lourd.

Un algorithme plus élégant fait intervenir une récursivité :

- Si l'arbre est vide, on renvoie un nouvel objet `Noeud` contenant la nouvelle valeur.
- Sinon, on compare la valeur à ajouter à celle du nœud sur lequel on est positionné :

    - si la valeur est inférieure à celle du nœud, on va modifier le sous-arbre gauche en le faisant _pointer_ vers ce même sous-arbre une fois que la valeur y aura été ajoutée, par un appel récursif.
    - si la valeur est supérieure à celle du nœud, on fait la même chose avec le sous-arbre de droite.
    - on renvoie le nouvel arbre ainsi créé.


Cela donne le code Python suivant :

{{ IDE("data/insertionABR_Recur")}}

## ^^3. Exercices et TP^^


* Télécharger le [carnet Jupyter](TP_Algo_ABR.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice : [supression d'une valeur dans un ABR](Algos_ABR_SuppressionValeur.pdf), à faire sur feuille.

* Exercice type Bac n°1 : [énoncé](Algo_ParcoursABR_SujetBac_ME2.pdf), à faire sur feuille.

* Exercice type Bac n°2 :

    * [énoncé](Algo_ParcoursABR_SujetBac_23_S0a.pdf), à faire sur feuille.
    * prolongement possible : coder la méthode de suppression du nœud correspondant à la tâche prioritaire décrite à la question 7.


