# Algorithmes de parcours d'un arbre :monkey:

Les arbres sont utilisés en informatique pour stocker des données de façon **hiérarchisée**.

Devant un arbre, devant un _stock de données_, l'une des premières choses que voudra faire un utilisateur est de pouvoir **parcourir** l'arbre pour lire les différentes données qui y sont stockées.

Devant un arbre de petite taille comme celui ci-dessous, notre œil arrive à tout percevoir d'un seul coup, mais cela n'est plus possible avec un arbre de taille très grande.

```mermaid
graph TD  
    T(T) --- Y(Y)
    T --- O(O)
    Y --- P(P)
    Y --- Q( )
    O --- H(H)
    O --- N(N)
```

Comme quand on se promène dans un labyrinthe, il faut se fixer une règle pour ne pas se perdre et ne pas oublier de visiter certains nœuds. 

Mais il n'y a pas une seule règle, il y en a plusieurs qui ont chacune leurs avantages et leurs inconvénients. On les appelle les **méthodes de parcours d'un arbre**.

> Remarque : dans la suite de cette page, l'expression "visiter un nœud" ne signifie pas "passer par le nœud" mais cela signifie "donner la valeur du nœud".

## ^^1. Parcours en largeur d'abord^^

En anglais :gb: :us: : *Breadth First Search (BFS)*

!!! tip "Méthode du parcours en largeur :heart:" 
    Le parcours en largeur d'abord est un parcours étage par étage (de haut en bas) et de gauche à droite.

![](data/BFS.png){: .center}

L'ordre des lettres parcourues est donc T-Y-O-P-H-N.

## ^^2. Parcours en profondeur d'abord^^

Imaginons une fourmi qui parcourt l'arbre de la façon suivante :

- elle part de la racine vers le sous-arbre de gauche,
- elle tourne à droite (:warning: _la fourmi a la tête vers le bas au départ_) à chaque croisement,
- elle fait demi-tour si elle est dans une impasse (sur une _feuille_),
- jusqu'à revenir à son point de départ (la _racine_).

![](data/parcoursProfondeur.png){: .center}

Elle dispose de trois possibilités pour lire les données d'un nœud : 

- lire un nœud quand elle le rencontre pour la première fois ;
- lire un nœud après avoir fini d'explorer le sous-arbre gauche du nœud ;
- lire un  nœud après avoir fini d'explorer les deux sous-arbres du nœud.

Ces trois possibilités correspondent à trois parcours que nous allons détailler maintenant, qui sont des parcours en **profondeur d'abord** (:gb: :us: *Depth First Search*). 


### 2.1. Parcours préfixe

!!! tip "Méthode du parcours préfixe :heart:"
    Le parcours se fait selon les règles suivantes :

    - chaque nœud est visité **avant** que ses fils le soient.
    - on part de la racine, puis on visite son fils gauche (et éventuellement le fils gauche de celui-ci, etc.) avant de remonter et de redescendre vers le fils droit.


![](data/prefixe.png){: .center}

L'ordre des lettres parcourues est donc T-Y-P-O-H-N.


### 2.2. Parcours infixe


!!! tip "Méthode du parcours infixe :heart:" 
    Le parcours se fait selon les règles suivantes :

    - chaque nœud est visité **après son fils gauche mais avant son fils droit**.
    - on part donc de la feuille la plus à gauche et on remonte par vagues sucessives. 
    
    :warning: Un nœud ne peut pas être visité si son fils gauche ne l'a pas encore été.

![](data/infixe.png){: .center}

L'ordre des lettres parcourues est donc P-Y-T-H-O-N.


### 2.3. Parcours postfixe

!!! tip "Méthode du parcours postfixe (ou suffixe) :heart:"
    Le parcours se fait selon les règles suivantes :

    - chaque nœud est visité **après** que ses deux fils le soient.
    - on part donc de la feuille la plus à gauche, et on ne remonte à un nœud père que si ses deux fils ont été visités. 

![](data/postfixe.png){: .center}

L'ordre des lettres parcourues est donc P-Y-H-N-O-T.


### 2.4. Comment ne pas se mélanger entre le pré / in / post fixe ?

Un pense-bête :

- *pré* correspond à *avant*
- *in* correspond à *au milieu*
- *post* correspond à *après*

Ces trois mots-clés parlent de la place du **père** par rapport à ses fils. 

- préfixe : le père doit être annoncé *avant* ses fils.
- infixe : le père doit être annoncé *au milieu* de son fils gauche (traité en premier) et de son fils droit.
- postfixe : le père ne doit être traité qu'*après* ses deux fils (celui de gauche d'abord, puis celui de droite).

Voici un moyen graphique pour bien distinguer ces trois parcours en profondeur :
![synthese](parcours_synthese.png){: .center}


### 2.5. Exercices de mise en pratique

!!! question "Exercice 1"
    === "Énoncé"
        Donner le rendu de **chacun des 4 parcours** de l'arbre binaire suivant :

        ```mermaid
        graph TD  
            M(1) --- N(2)
            M --- O(3)
            N --- P(4)
            N --- Q(5 )
            O --- V1( )
            O --- R(6)
            Q --- S(7)
            Q --- T(8)
            R --- U(9)
            R --- V2( )
        ```

    === "Corr. largeur"
        largeur : 1 2 3 4 5 6 7 8 9
    === "Corr. préfixe"
        préfixe : 1 2 4 5 7 8 3 6 9
    === "Corr. infixe"
        infixe : 4 2 7 5 8 1 3 9 6
    === "Corr. postfixe"
        postfixe : 4 7 8 5 2 9 6 3 1



!!! question "Exercice 2"
    === "Énoncé"
        Donner le rendu de **chacun des 4 parcours** de l'arbre binaire suivant :

        ```mermaid
        graph TD  
            M(9) --- N(8)
            M --- O(7)
            N --- P(6)
            N --- Q(2 )
            O --- V1( )
            O --- R(5)
            Q --- S(1)
            Q --- V2( )
            R --- U(4)
            R --- T(3)
        ```

    === "Corr. largeur"
        largeur : 9 8 7 6 2 5 1 4 3
    === "Corr. préfixe"
        préfixe : 9 8 6 2 1 7 5 4 3
    === "Corr. infixe"
        infixe : 6 8 1 2 9 7 4 5 3
    === "Corr. postfixe"
        postfixe : 6 1 2 8 4 3 5 7 9


## ^^3. Algorithmes de parcours et codage en Python^^

> Remarque préliminaire : bien que le parcours en largeur soit le plus simple à expliquer sur un schéma, il est plus difficile à décrire par un algorithme. Il ne sera traité qu'en exercice.

Nous allons traduire sous forme d'algorithmes les différentes méthodes de parcours en profondeur rencontrées plus haut : tous ces algorithmes exploiteront la structure **récursive** d'un arbre binaire.


Pour la programmation en Python, on utilisera l'implémentation en Programmation Orientée Objet (la _version simple_ vue dans le [chapitre dédié](https://pasallard.gitlab.io/terminale_nsi_voltaire/Structures/2_Arbres/2_ArbresBinaires/Structures_ArbresBinaires/){target="_blank"}), rappelée ici :

```python
class Noeud:
    def __init__(self, val = None, sag = None, sad = None):
        self.valeur = val
        self.left = sag     # sous-arbre gauche
        self.right = sad    # sous-arbre droit

```


### 3.1. Algorithme du parcours préfixe

!!! tip "Parcours préfixe :heart:"
    Puisque le parcours préfixe consiste à lire la valeur d'un nœud avant d'explorer le sous-arbre gauche puis le sous-arbre droit, un **algorithme récursif de parcours préfixe** est :

        FONCTION ParcoursPrefixe(arbreBinaire)

            SI est_vide(arbreBinaire) ALORS
                RENVOYER Rien # cas de base, on ne fait rien sur un arbre vide
            SINON
                AFFICHER valeur(arbreBinaire)
                ParcoursPrefixe(sous_arbre_gauche(arbreBinaire))
                ParcoursPrefixe(sous_arbre_droit(arbreBinaire))
            FIN_SI


Coder cet algorithme en Python en utilisant l'implémentation Orientée Objet d'un arbre.

{{ IDE("data/parcoursPrefixePOO") }}

Le test proposé concerne l'arbre de l'exemple de cours (partie 2 ci-dessus) :

```mermaid
graph TD  
    T(T) --- Y(Y)
    T --- O(O)
    Y --- P(P)
    Y --- Q( )
    O --- H(H)
    O --- N(N)
```

??? danger "Solution"

    Un code possible est :

    ```python
    def parcoursPrefixe(arbreBinaire):
        if arbreBinaire is None :
            return None
        print(arbreBinaire.valeur, end = "-")
        parcoursPrefixe(arbreBinaire.left)
        parcoursPrefixe(arbreBinaire.right)
    ```

    On doit obtenir à l'affichage : `T-Y-P-O-H-N`.

### 3.2. Algorithme du parcours infixe

!!! tip "Parcours infixe :heart:"
    Seul changement par rapport à l'algorithme précédent : la position de l'instruction de lecture de la donnée du nœud. 

        FONCTION ParcoursInfixe(arbreBinaire)

            SI est_vide(arbreBinaire) ALORS
                RENVOYER Rien # cas de base, on ne fait rien sur un arbre vide
            SINON
                ParcoursInfixe(sous_arbre_gauche(arbreBinaire))
                AFFICHER valeur(arbreBinaire)
                ParcoursInfixe(sous_arbre_droit(arbreBinaire))
            FIN_SI


Coder cet algorithme en Python en utilisant l'implémentation Orientée Objet d'un arbre.

{{ IDE("data/parcoursInfixePOO")}}

??? warning "Solution"

    ```python
    def parcoursInfixe(arbreBinaire):
        if arbreBinaire is None :
            return None
        parcoursInfixe(arbreBinaire.left)
        print(arbreBinaire.valeur, end = "-")
        parcoursInfixe(arbreBinaire.right)
    ```

    On doit obtenir à l'affichage : `P-Y-T-H-O-N`.


### 3.3. Parcours postfixe

!!! tip "Parcours postfixe :heart:"
    Là encore, le seul changement par rapport à l'algorithme précédent est la position de l'instruction de lecture de la donnée du nœud. 

        FONCTION ParcoursPostfixe(arbreBinaire)

            SI est_vide(arbreBinaire) ALORS
                RENVOYER Rien # cas de base, on ne fait rien sur un arbre vide
            SINON
                ParcoursPostfixe(sous_arbre_gauche(arbreBinaire))
                ParcoursPostfixe(sous_arbre_droit(arbreBinaire))
                AFFICHER valeur(arbreBinaire)
            FIN_SI


Coder cet algorithme en Python en utilisant l'implémentation Orientée Objet d'un arbre.

{{ IDE("data/parcoursPostfixePOO")}}

??? warning "Solution"

    ```python
    def parcoursPostfixe(arbreBinaire):
        if arbreBinaire is None :
            return None
        parcoursPostfixe(arbreBinaire.left)
        parcoursPostfixe(arbreBinaire.right)
        print(arbreBinaire.valeur, end = "-")
    ```

    On doit obtenir à l'affichage : `P-Y-H-N-O-T`.


!!! info "Pause vidéo" 

    - Regardez et appréciez [cette vidéo](https://youtu.be/OTfp2_SwxHk){. target="_blank"}
    - À l'aide de la vidéo, codez le parcours infixe en itératif (_niveau avancé !_)

## ^^4. Exercices et TP^^

* Généralités : 
    - exercices sur feuille : télécharger le [document](Exos_AlgoParcours_Arbres.pdf).
    - TP : télécharger le [carnet Jupyter](TP_Algo_ArbresParcours.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : [énoncé](Algo_ParcoursArbres_SujetBac_ME1.pdf).

* Exercice type Bac n°2:
    
    - [énoncé](Algo_ParcoursArbres_SujetBac_LR1.pdf), à faire d'abord sur feuille 
    - puis mise en œuvre sur un [carnet Jupyter](Algo_ParcoursArbres_SujetBac_LR1.ipynb).

* Exercice type Bac n°3 (implémentation d'un arbre binaire par un dictionnaire):
    
    - [énoncé](DictionnairesArbres_BacAmSud_2022.pdf), à faire d'abord sur feuille 
    - puis mise en œuvre sur un [carnet Jupyter](DictionnairesArbres_BacAmSud_2022.ipynb).