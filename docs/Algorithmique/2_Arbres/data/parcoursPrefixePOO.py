class Noeud:
    def __init__(self,val = None, sag = None, sad = None) :
        self.valeur = val
        self.left = sag
        self.right = sad

def parcoursPrefixe(arbreBinaire) :
    """Décrit le parcours préfixe d'un arbre binaire
    Entrée : un arbre binaire codé par un objet de la classe Noeud
    Sortie : rien, juste un affichage"""
    pass # compléter ici
    
# un test    
monArbre = Noeud("T", Noeud("Y", Noeud("P"), None), Noeud("O", Noeud("H"), Noeud("N")))
parcoursPrefixe(monArbre)