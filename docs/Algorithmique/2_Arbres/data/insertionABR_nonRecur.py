#--- HDR ---#
class Noeud:
    def __init__(self,val = None, sag = None, sad = None) :
        self.valeur = val
        self.left = sag
        self.right = sad

def parcoursInfixe(arbreBinaire) :
    """Décrit le parcours infixe d'un arbre binaire
    Entrée : un arbre binaire codé par un objet de la classe Noeud
    Sortie : rien, juste un affichage"""
    if arbreBinaire is None :
        return None
    parcoursInfixe(arbreBinaire.left)
    print(arbreBinaire.valeur, end = "-" )
    parcoursInfixe(arbreBinaire.right)
#--- HDR ---#
def insertion(abr, val) :
    """Insère la valeur val dans l'ABR codé à l'aide d'un objet de la classe Noeud"""
    bonEndroit = False
    noeud_temp = abr
    while not bonEndroit :
        if val < noeud_temp.valeur : # on regarde alors du côté gauche
            if noeud_temp.left is None : # il y a une place pour y mettre la nouvelle feuille
                bonEndroit = True
                noeud_temp.left = Noeud(val) # on ajoute la nouvelle feuille du bon côté
            else : # on descend du côté gauche
                noeud_temp = noeud_temp.left
        else :
            if noeud_temp.right is None : # il y a une place pour y mettre la nouvelle feuille
                bonEndroit = True
                noeud_temp.right = Noeud(val)
            else : # on descend du côté droit
                noeud_temp = noeud_temp.right

# un test
monABR = Noeud(25,Noeud(19, Noeud(7), Noeud(24)),Noeud(37, Noeud(29), Noeud(52)))
insertion(monABR, 20)
print("parcours infixe de l'ABR après ajout de la valeur 20 :")
parcoursInfixe(monABR) # le parcours infixe d'un ABR doit donner une suite croissante
print("\nparcours infixe de l'ABR après ajout de la valeur 40 :")
insertion(monABR, 40)
parcoursInfixe(monABR)
