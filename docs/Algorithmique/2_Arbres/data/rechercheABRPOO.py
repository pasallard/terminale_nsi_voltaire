class Noeud:
    def __init__(self,val = None, sag = None, sad = None) :
        self.valeur = val
        self.left = sag
        self.right = sad

def Contient_valeur(abr, val) :
    """ renvoie True si val est dans l'ABR et False sinon"""
    pass # compléter ici
            
# quelques tests   
monABR = Noeud(25,Noeud(19, Noeud(7), Noeud(24)),Noeud(37, Noeud(29), Noeud(52)))
assert Contient_valeur(monABR, 37)  == True, "problème de code 1"
assert Contient_valeur(monABR, 15)  == False, "problème de code 2"