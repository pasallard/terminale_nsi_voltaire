
# Exercices et Travaux Pratiques en SQL

## A - QCM


{{ multi_qcm(
    [
        "De quoi SQL est-il l'acronyme ?",
        ["Strong Query Language", "Strong Question Language", "Structured Question Language", "Structured Query Language"],[4],
    ],
    [
        "Quel mot-clé SQL est utilisé pour ==rechercher== des données dans une base de données ?",
        ["FIND", "GET", "SEARCH", "SELECT"],[4],
    ],
    [
        "Quel mot-clé SQL est utilisé pour ==modifier== des données dans une base de données ?",
        ["MODIFY", "REPLACE", "UPDATE", "CHANGE"],[3],
    ],
    [
        "Quel mot-clé SQL est utilisé pour ==ajouter== de nouvelles données dans une base de données ?",
        ["ADD", "APPEND", "INSERT", "RECORD"],[3],
    ],
    [
        "Quel mot-clé SQL est utilisé pour ==supprimer== des données dans une base de données ?",
        ["DELETE", "SUPPRESS", "CANCEL", "REMOVE"],[1],
    ],
    [
        "Quel(s) mot(s)-clé(s) SQL est utilisé pour ==trier== les résultats d'une requête ?",
        ["ORDER", "ORDER BY", "SORT", "SORT BY"],[2],
    ]
    )}}




## B - Travaux Pratiques 

!!! info "Memento sur les requêtes de base du langage SQL"

    Quelques exemples de syntaxe SQL :
    
    - Insérer un nouvel enregistrement :
        - `INSERT INTO Table VALUES (valeur1 , valeur2, valeur3, ...);`
    - Modifier un ou plusieurs enregistrements :
        - `UPDATE Table SET attribut1=valeur1 WHERE Selecteur;`
    - Supprimer des enregistrements :
        - `DELETE FROM Table WHERE Selecteur;`
    - Sélectionner des enregistrements :
        - `SELECT attributs FROM Table WHERE Selecteur;`
    - Sélectionner des enregistrements dans un ordre ascendant :
        - `SELECT attributs FROM Table WHERE Selecteur ORDER BY attribut ASC;`
    - Sélectionner des enregistrements sans doublon :
        - `SELECT DISTINCT attributs FROM Table WHERE Selecteur;`

    ![](schema_SELECT.png)

### ^^TP 1 : les Jeux Olympiques^^

Télécharger la base de données des [médailles olympiques de 1976 à 2008](olympics1976-2008.db) et l'ouvrir avec [Basthon](https://notebook.basthon.fr/?kernel=sql){target="_blank"} ou un autre outil de votre choix.

Cette base de donnée est composée d'une seule table ==`Medals`==, dont les attributs sont :

<figure markdown>

Attributs | Type
:---|:---
Id | INTEGER 🔑
City | TEXT
Year | INTEGER
Sport | TEXT
Discipline | TEXT
Event | TEXT
Athlete | TEXT
Gender | TEXT
Country_code | TEXT
Country | TEXT
Event_Gender | TEXT
Medal | TEXT

</figure>

    

**1.** Tester chacune des requêtes suivantes en observant bien les résultats renvoyés, puis **exprimer en français de tous les jours la nature de cette requête**.

**a)**
```sql
    SELECT * FROM Medals WHERE Year=2008 AND Country="France";
```

**b)**
```sql
    SELECT City, Year, Athlete FROM Medals WHERE Medal="Gold" AND Event="110m hurdles";
```

**c)*** 

```sql
    SELECT Athlete, Event FROM Medals WHERE Medal="Gold" AND Country="France" AND Year=2000 ORDER BY Athlete ASC;
```

**d)**
```sql
    SELECT DISTINCT Country FROM Medals WHERE Medal="Gold" AND YEAR="1984" ORDER BY Country DESC;
```

**e)**

```sql
    SELECT Athlete, City, Year, Medal, Country FROM Medals WHERE Country LIKE "%nia";
```

> _Remarque_ : en SQL, on utilise le symbole `%` pour désigner n'importe quelle série de caractères (0, 1 ou plusieurs caractères) ; il a le même rôle que le symbole `*` dans le monde Linux.

**2.** Écrire les requêtes SQL permettant d'extraire de cette base de données les informations suivantes :

**a.** La liste _sans doublons_ des noms de tous les athlètes français ayant obtenu une médaille d'or aux jeux olympiques de 1984.

**b.** Les noms de toutes les athlètes féminines ayant obtenu une médaille d'or au marathon et l'année d'obtention de cette médaille, avec un classement chronologique.

**c.** La liste de tous les athlètes qui ont le même prénom que vous, avec la nature de leur médaille et leur discipline  (_la requête doit se terminer par `WHERE Athlete LIKE "%Pierre%"` si votre prénom est Pierre_).

**d.** Les épreuves dans lesquelles le champion de natation américain [Michael Phelps](https://fr.wikipedia.org/wiki/Michael_Phelps){target="_blank"} a obtenu des médailles en 2004.

**e.** Le nombre total de médailles d'or et d'argent obtenues par l'Australie en natation.

**3.** Pour aller plus loin : on peut créer un tableau de synthèse avec la clause `GROUP BY`. Tester la requête suivante :

```sql
SELECT Country, COUNT(*) FROM Medals WHERE Discipline="Swimming" AND Medal="Gold" GROUP BY Country ORDER BY COUNT(*) DESC;
```

Écrire une requête qui dresse le tableau des épreuves dans lesquelles la France a obtenu des médailles d'or, avec classement par ordre décroissant de nombres de médailles d'or.

??? danger "Solution"

    ```sql
    SELECT Event, COUNT(*) FROM Medals WHERE Country = "France" AND Medal = "Gold" GROUP BY Event ORDER BY COUNT(*) DESC
    ```

!!! Attention
    La clause  `GROUP BY` n'est pas exigible en terminale NSI et ne fera pas partie des évaluations.


### ^^TP 2 : les Prix Nobel^^

**1.** Télécharger la base de données des [Prix Nobel](Nobel.db) et l'ouvrir avec l'ouvrir avec [Basthon](https://notebook.basthon.fr/?kernel=sql){target="_blank"} ou un autre outil de votre choix.

Pour prendre connaissance des attributs et de leur signification, saisir `SELECT * FROM nobel LIMIT 10`. 


**2.**. Écrire les requêtes SQL permettant :

**a)** d'obtenir les catégories dans lesquelles sont attribuées les prix Nobel ;
    
**b)** de lister par ordre alphabétique ( _sans doublon_) les lauréats du prix Nobel nés en France ou travaillant pour une organisation Française ;

**c)** de lister les années où le "Comité international de la Croix Rouge" a obtenu le prix Nobel ;

**d)** de connaître le nombre de femmes ayant obtenu un prix Nobel ;

**e)** de lister les femmes françaises ayant obtenu un prix Nobel depuis 2005 ainsi que la catégorie de ce prix Nobel ;

**f)** de lister par âge décroissant les lauréats individuels du prix Nobel qui sont toujours en vie (leur _DeathYear_ est alors _NULL_) ;

**g)** de rechercher les lauréats dont le nom contient "Curie".


### ^^TP 3 : les joueurs de basket évoluant en NBA^^ :basketball:

On a déjà utiliser le mot-clé `COUNT` pour le nombre d'enregistrements vérifiant certains critères.

On peut aussi utiliser les mots-clés `MIN`, `MAX`, `SUM` et `AVG` (:flag_gb: :flag_us: _Average_ ) pour obtenir respectivement le minimum, le maximum, la somme et la moyenne de __champs/attributs numériques__.

**1.** Télécharger la base de données des [joueurs de NBA](nba.db) (saison 2016) et l'ouvrir avec [Basthon](https://notebook.basthon.fr/?kernel=sql){target="_blank"} ou un autre outil de votre choix.

Cette base de données est composée d'une seule table, dont le schéma relationnel est le suivant :

> joueurs_nba (<u>Name</u>, Team, Number, Position, Age, Height, Weight)


La taille (_Height_) d'un joueur est donné en cm et son poids (_Weight_) est donné en kg. 
L'attribut `Number` correspond au numéro que le joueur porte sur son maillot et l'attribut `Position` est l'abréviation de son poste sur un terrain de basket.

![](Basketball_Positions.png){: .center width 60%}


Pour visualiser les premiers enregistrements de cette relation, saisir `SELECT * FROM joueurs_nba`. 

**2.** Tester la requête SQL suivante et exprimer en français de tous les jours la nature de cette requête.

```sql 
SELECT AVG(Age) FROM joueurs_nba WHERE Team = "Los Angeles Lakers";
```

**3.** Écrire les requêtes SQL permettant :

**a)** d'obtenir la taille maximum des joueurs de l'équipe des Chicago Bulls ;

**b)** d'obtenir le poids le plus faible de tous les joueurs de NBA évoluant au poste de Shooting Guard ;

**c)** d'obtenir toutes les informations concernant Tony Parker puis de savoir s'il est le plus agé des joueurs de NBA évoluant au poste de Point Guard (_faire deux requêtes séparées_) ;


**4.** Modification de la base de données


**d)** Écrire la requête SQL permettant de vous ajouter dans la base des données des joueurs de NBA, au sein de l'équipe des Boston Celtics avec le numéro 99 et au poste de Power Forward (_ou bien tout autre valeur de votre choix_)  ;

> Dans l'onglet `Parcourir les données`, vérifier ensuite que la dernière ligne correspond à votre enregistrement.

**e)** On suppose que Joakim Noah, qui évoluait dans l'équipe des Chicago Bulls, va rejoindre Tony Parker chez les San Antonio Spurs où il portera alors le numéro 75. Écrire la ou les requête(s) qui permet(tent) de mettre à jour la base de données.

Vérifier ensuite par une requête du type `SELECT * FROM joueurs_nba WHERE Name = 'Joakim Noah'` que la mise à jour a bien été effectuée.

**f)** Un plaisantin a pollué la table de données en insérant une équipe `Gotham City` composée de trois joueurs `Batman`, `Robin` et `Wonder Woman`. Écrire la requête SQL permettant de supprimer ces trois enregistrements.



## C - Exercices type Bac


### Exercice n°1

Pour mettre en place une base de données dans une médiathèque, on décide de créer une table contenant les informations sur les livres. On donne ci-dessous la représentation de cette table avec ses deux premiers enregistrements.

|Titre|Auteur|Pays|Année|
|-----|------|----|-----|
|Les misérables|Victor Hugo|France|1862|
|1984|George Orwell | Angleterre | 1949|

!!! question "Questions"

    1. Quel est le schéma relationnel de cette table ?
    2. Proposer un type pour l'attribut `Année`
    3. Certains livres sont achetés en plusieurs exemplaires dans cette médiathèque : expliquer pourquoi le modèle de table choisi ci-dessus ne convient plus et proposer une correction.

??? danger "Réponse"

    1. Le schéma relationnel de cette table est  `(Titre, Auteur, Pays, Année)`, ou plus précisément `(Titre:TEXT, Auteur:TEXT, Pays: TEXT, Année:INT)`.
    2. Un type pour l'attribut `Année` pourrait être INT.
    3. Avec ce modèle de table, il n'est pas possible de gérer deux exemplaires du même ouvrage car il ne peut pas y avoir de doublons dans une table. On doit modifier la table en ajoutant un attribut, par exemple un `Identifiant`.

### Exercice type Bac n°2 (d'après 2022, Polynésie, Jour 1)


!!! info "SQL"
    L'énoncé de cet exercice peut utiliser les mots du langage SQL suivants :

    `SELECT, FROM, WHERE, INSERT INTO, VALUES, UPDATE, SET, DELETE, COUNT, DISTINCT, AND, OR, ORDER BY, ASC, DESC`

Un site web recueille des données de navigation dans une base de données afin d'étudier les profils de ses visiteurs.  
Chaque requête d'interrogation d'une page de ce site est enregistrée dans une première table dénommée **`Visites`** sous la forme d'un 5-uplet : `(identifiant, adresseIP, date et heure de visite, nom de la page, navigateur)`.

Le chargement de la page `index.html` par `192.168.1.91` le 12 juillet 1998 à 22 h 48 aura par exemple été enregistré de la façon suivante :

`(1534, "192.168.1.91", "1998-07-12 22:48:00", "index.html", "Internet explorer 4.1")`.

Un extrait de cette table vous est donné ci-dessous :

|identifiant | ip | dateheure | nomPage | navigateur |
|:---|:---|:---|:---|:---|
| ... | ... | ... | ... | ... |
| 1534 | `"192.168.1.91"` | `"1998-07-12 22:48:00"`  | `"index.html"` | `"Internet explorer 4.1"` |
| 1535 | `"192.168.1.91"` | `"1998-07-12 22:49:05"`  | `"exercices.html"` | `"Internet explorer 4.1"` |
| 1536 | `"192.168.1.151"` | `"1998-07-12 22:59:44"`  | `"index.html"` | `"Netscape 6"` |
| 1537 | `"192.168.1.151"` | `"1998-07-12 23:00:00"`  | `"espace_enseignant.html"` | `"Netscape 6"` |
| 1538 | `"192.168.1.91"` | `"1998-07-12 23:29:00"`  | `"icorrection.html"` | `"Internet explorer 4.1"` |
| ... | ... | ...  | ... | ... |

**1.a.** Donner une commande d'interrogation en langage SQL permettant d'obtenir l'ensemble des couples `(adresse IP, nom de la page)` de cette table.

??? danger "Réponse"

    ```sql
    SELECT ip, nomPage FROM Visites;
    ```


**1.b.** Donner une commande en langage SQL permettant d'obtenir l'ensemble des adresses IP ayant interrogé le site, sans doublon.

??? danger "Réponse"

    ```sql
    SELECT DISTINCT ip FROM Visites;
    ```


**1.c.** Donner une commande en langage SQL permettant d'obtenir la liste des noms des pages visitées par l'adresse IP `192.168.1.91`

??? danger "Réponse"

    ```sql
    SELECT nomPage FROM Visites WHERE ip = '192.168.1.91';
    ```

Ce site web met en place, sur chacune de ses pages, un programme en JavaScript qui envoie au serveur, à intervalle régulier de 15 secondes, le temps en secondes (`duree`) de présence sur la page. Ces envois contiennent tous la valeur de `identifiant` correspondant au chargement initial de la page.  
Par exemple, si le visiteur du 12 juillet 1998 est resté 65 secondes sur la page, celle-ci a envoyé au serveur les 4 couples `(1534, 15)`, `(1534, 30)`, `(1534, 45)` et `(1534, 60)`.

Ces données sont enregistrées dans une table nommée **`Pings`**.

En plus de l'inscription d'une ligne dans la table **`Visites`**, chaque chargement d'une nouvelle page provoque l'insertion d'une ligne dans la table **`Pings`** comprenant l'identifiant de ce chargement et une durée de `0`.

Enfin, chaque ligne de la table **`Pings`** est unique, et ses deux colonnes contiennent toujours un `identifiant` et une `duree`.

L'attribut `identifiant` de la table **`Pings`** fait référence à l'attribut du même nom de la table **`Visites`** et les deux partagent les mêmes valeurs.

Un extrait de cette table vous est donné ci-dessous :

|Identifiant | duree |
|:---|:---|
| ... | ... |
| 1534 | 0 |
| 1534 | 15 |
| 1534 | 30 |
| 1534 | 45 |
| 1534 | 60 |
| ... | ... |
| 1536 | 0 |
| 1537 | 0 |
| 1537 | 15 |
| ... | ... |


**2.** Le serveur reçoit le couple `(identifiant, duree)` suivant : `(1534, 105)`. Écrire la commande SQL d'insertion qui permet d'ajouter cet enregistrement à la table **`Pings`**.

??? danger "Réponse"

    ```sql
    INSERT INTO Pings VALUES (1534, 105);
    ```


On envisage ensuite d'optimiser la table en se contentant d'une seule ligne par identifiant dans la table **`Pings`** : les valeurs de l'attribut `duree` devraient alors être mises à jour à chaque réception d'un nouveau couple `(identifiant, duree)`.

**4.** Écrire la requête de mise à jour permettant de fixer à 120 la valeur de l'attribut `duree` associée à l'identifiant 1534 dans la table **`Pings`**.

??? danger "Réponse"

    ```sql
    UPDATE Pings SET duree = 120 WHERE identifiant = 1534;
    ```



### Exercice type Bac n°3 (d'après 2022, Centres étrangers, Jour 1)


!!! info "Rappel sur le langage SQL"
    Types de données

    | Type         | Description                                                                            |
    | ------------ | -------------------------------------------------------------------------------------- |
    | `CHAR(N)`    | Texte de N caractères exactement                                                          |
    | `VARCHAR(N)` | Texte de N caractères au maximum                                                         |
    | `TEXT`       | Texte de 65535 caractères maximum                                                  |
    | `INT`        | Nombre entier de -2^31^ à 2^31^ - 1 (signé) ou de 0 à 2^32^ - 1 (non signé) |
    | `FLOAT`      | Réel à virgule flottante (approximation)                                               |
    | `DATE`       | Date format `AAAA-MM-JJ`                                                               |



Dans le cadre d'une étude sur le réchauffement climatique, un centre météorologique rassemble des données. On considère que la base de données contient deux relations (tables) :

- la relation **`Centres`**,  qui contient l'identifiant des centres météorologiques, la ville, la latitude, la longitude et l'altitude du centre ;
- la relation **`Mesures`** qui contient l'identifiant de la mesure, l'identifiant du centre, la date de la mesure, la température, la pression et la pluviométrie mesurées.

Le schéma relationnel de la relation **`Centres`** est le suivant :

`Centres(id_centre: INT, nom_ville: VARCHAR, latitude: FLOAT, longitude: FLOAT, altitude: FLOAT)`

Le schéma relationnel de la relation **`Mesures`** est le suivant :

`Mesures(id_mesure: INT, id_centre: INT, date_mesure: DATE, temperature: FLOAT, pression: INT, pluviometrie: INT)`

On fournit ci-dessous le contenu des deux relations.

!!! abstract "Relation **`Centres`**"

    | `id_centre` | `nom_ville`     | `latitude` | `longitude` | `altitude` |
    | ----------: | :-------------- | ---------: | ----------: | ---------: |
    |         213 | Amiens          |     49.894 |       2.293 |         60 |
    |         138 | Grenoble        |     45.185 |       5.723 |        550 |
    |         263 | Brest           |     48.388 |       -4.49 |         52 |
    |         185 | Tignes          |     45.469 |       6.909 |       2594 |
    |         459 | Nice            |     43.706 |       7.262 |        260 |
    |         126 | Le Puy-en-Velay |     45.042 |       3.888 |        744 |
    |         317 | Gérardmer       |     48.073 |       6.879 |        855 |


!!! abstract "Relation **`Mesures`**"

    | `id_mesure` | `id_centre` | `date_mesure` | `temperature` | `pression` | `pluviometrie` |
    | ----------: | ----------: | :------------ | ------------: | ---------: | -------------: |
    |        1566 |         138 | 2021-10-29    |           8.0 |       1015 |              3 |
    |        1568 |         213 | 2021-10-29    |          15.1 |       1011 |              0 |
    |        2174 |         126 | 2021-10-30    |          18.2 |       1023 |              0 |
    |        2200 |         185 | 2021-10-30    |           5.6 |        989 |             20 |
    |        2232 |         459 | 2021-10-31    |          25.0 |       1035 |              0 |
    |        2514 |         213 | 2021-10-31    |          17.4 |       1020 |              0 |
    |        2563 |         126 | 2021-11-01    |          10.1 |       1005 |             15 |
    |        2592 |         459 | 2021-11-01    |          23.3 |       1028 |              2 |
    |        3425 |         317 | 2021-11-02    |           9.0 |       1012 |             13 |
    |        3430 |         138 | 2021-11-02    |           7.5 |        996 |             16 |
    |        3611 |         263 | 2021-11-03    |          13.9 |       1005 |              8 |
    |        3625 |         126 | 2021-11-03    |          10.8 |       1008 |              8 |


**1.** Proposer une clé primaire pour la relation **`Mesures`**. Justifier votre choix.

??? danger "Réponse"
    
    L'attibut `id_mesure` permet d'identifier de manière unique chaque mesure de la table : c'est **la** bonne clé primaire pour cette relation.

    Par contre, pour les autres attributs :

    - `id_centre` n'est pas unique, il ne peut pas servir de clé primaire.
    - `date_mesure` et `pluviometrie` non plus.
    - `temperature` et `pression` sont uniques pour l'instant, mais probablement pas ensuite : c'est un mauvais choix.

    

**2.a.** Qu'affiche la requête suivante ?

```sql
SELECT * FROM Centres WHERE altitude > 500;
```

??? danger "Réponse"
    La requête affiche tous les champs de la table **`Centres`** pour lesquels l'altitude est strictement supérieure à 500 m.


    | `id_centre` | `nom_ville`     | `latitude` | `longitude` | `altitude` |
    | ----------: | :-------------- | ---------: | ----------: | ---------: |
    |         138 | Grenoble        |     45.185 |       5.723 |        550 |
    |         185 | Tignes          |     45.469 |       6.909 |       2594 |
    |         126 | Le Puy-en-Velay |     45.042 |       3.888 |        744 |
    |         317 | Gérardmer       |     48.073 |       6.879 |        855 |


**2.b.** On souhaite récupérer le nom de la ville des centres météorologiques situés à une altitude comprise entre 700 m et 1200 m. Écrire la requête SQL correspondante.

??? danger "Réponse"

    ```sql
    SELECT nom_ville FROM Centres WHERE altitude >= 700 AND altitude <= 1200;
    ```

    | `nom_ville`     |
    | :-------------- |
    | Le Puy-en-Velay |
    | Gérardmer       |

**2.c.** On souhaite récupérer la liste des longitudes et des noms des villes des centres météorologiques dont la longitude est supérieure à 5. La liste devra être triée par ordre alphabétique des noms de ville. Écrire la requête SQL correspondante.

??? danger "Réponse"

    ```sql
    SELECT longitude, nom_ville
    FROM Centres
    WHERE longitude > 5
    ORDER BY nom_ville ASC;
    ```

    | `longitude` | `nom_ville` |
    | ----------: | :---------- |
    |       6.879 | Gérardmer   |
    |       5.723 | Grenoble    |
    |       7.262 | Nice        |
    |       6.909 | Tignes      |



**3.a.** Qu'affiche la requête suivante ?

```sql
SELECT * FROM Mesures WHERE date_mesure = "2021-10-30";
```

??? danger "Réponse"
    La requête affiche tous les champs des enregistrements de la table **`Mesures`** pour la date du 30 octobre 2021.


    | `id_mesure` | `id_centre` | `date_mesure` | `temperature` | `pression` | `pluviometrie` |
    | ----------: | ----------: | :------------ | ------------: | ---------: | -------------: |
    |        2174 |         126 | 2021-10-30    |          18.2 |       1023 |              0 |
    |        2200 |         185 | 2021-10-30    |           5.6 |        989 |             20 |


**3.b.** Écrire une requête SQL permettant d'ajouter une mesure prise le 8 novembre 2021 dans le centre numéro 138, où la température était de 11 °C, la pression de 1013 hPa et la pluviométrie de 0 mm. La donnée dont l'attribut est `id_mesure` aura pour valeur 3650.

??? danger "Réponse"

    ```sql
    INSERT INTO Mesures
    VALUES (3650, 138, 2021-11-08, 11, 1013, 0);
    ```


**4.** Expliquer ce que renvoie la requête SQL suivante ?

```sql
SELECT * FROM Centres WHERE latitude = (SELECT MIN(latitude) FROM Centres);
```

??? danger "Réponse"
    La requête imbriquée `#!sql SELECT MIN(latitude) FROM Centres;` renvoie `43.706` qui est la plus petite latitude parmi celle de la table **`Centres`**. Elle correspond à la latitude de Nice, la ville la plus au Sud de cette table.

    Cette requête renvoie donc tous les champs du centre situé le plus au sud parmi ceux de la table **`Centres`**.


    | `id_centre` | `nom_ville` | `latitude` | `longitude` | `altitude` |
    | ----------: | :---------- | ---------: | ----------: | ---------: |
    |         459 | Nice        |     43.706 |       7.262 |        260 |
