# Stéganographie 🕵🏽‍♀️

La stéganographie consiste à **cacher un texte dans une image**. 

Il ne s'agit pas de l'écrire en tout petit, mais vraiment de l'enfouir profondément dans la texture de l'image. Le texte est totalement invisible, sauf si on sait qu'il est là, et qu'on sait comment le lire....

On peut ainsi envoyer un message secret à un espion en déposant une image sur un réseau social, sur un forum, etc. L'espion consulte régulièrement vos posts mais il est le seul à savoir que l'image contient un message et il sait la décoder !


## ^^1. Principe de la stéganographie^^ 

_Si besoin : quelques rappels sur la numérisation des images [dans le TP Traitement d'images](https://pasallard.gitlab.io/terminale_nsi_voltaire/Programmation/3_Modularite/TP_Traitement_Image/Modularite_TP_Traitement_Image/) du début d'année_.


!!! success "Exemple introductif"
    Alice veut envoyer à Bob le message suivant : "6" , c'est-à-dire `110` en binaire. 

    Elle prend une photo quelconque et constate que le premier pixel de l'image a une couleur <span style="background-color: #483d8b "> &nbsp; &nbsp; &nbsp; &nbsp; </span> &nbsp; de code RGB (72, 61, 139), soit en binaire (01001000, 00111101, 10001011).

    Elle modifie légèrement ce pixel en lui donnant la couleur <span style="background-color: #493d8a "> &nbsp; &nbsp; &nbsp; &nbsp;  </span> &nbsp; de code RGB (73, 61, 138), soit en binaire (0100100**1**, 0011110**1**, 1000101**0**). Puis elle poste cette image modifiée.

    Visuellement, l'oeil humain ne parvient pas à faire la différence et personne ne peut se rendre compte d'une altération de l'image. 

    Pourtant, juste en modifiant les trois bits de poids faible du code RGB, Alice a réussit à y inscrire le nombre 6 en binaire !



!!! tip "Principe de la stéganographie"
    Le principe de la stéganographie est donc le suivant :

    1. coder le message secret sous forme binaire (grâce au codage ASCII ou UTF-8), pour obtenir une succession de 0 et de 1 qui représente le message secret ;
    2. mettre chacun de ces 0 et 1 à la place du bit de poids faible de chaque couleur de chaque pixel d'une image anodine ;
    3. pour faciliter le décodage, on codera aussi au départ la longueur du message selon le même principe.

    ![schema](schemaStegano.png){: .center}

    Pour décoder, il faudra faire les opérations dans le sens inverse :

    1. décoder la longueur du message, pour savoir à quel pixel on arrête de décoder ;
    2. lire chaque pixel et récupérer le bit de poids faible de chaque couleur ;
    2. les assembler bout à bout pour former une succession de 0 et de 1, que l'on convertira ensuite en texte selon le codage ASCII.


## ^^2. Quelques outils en Python pour la mise en œuvre de la stéganographie^^

!!! info "Codage ASCII/UTF-8"
    * On peut obtenir le code ASCII/UTF-8 d'un caractère avec la fonction `ord`. Saisir par exemple dans la console `ord('A')` : cela doit renvoyer 65, qui est le code ASCII du caractère A.

    {{terminal()}}

    * Inversement, à partir d'un code ASCII, on obtient le caractère correspondant avec la fonction `chr` (_character_). Saisir par exemple en console `chr(65)`, qui doit renvoyer le caractère `'A'`.


Ces deux fonctions `ord` et `chr` manipulent les nombres avec une écriture classique, en base 10. Or nous avons besoin de les convertir en binaire : vous vous souvenez sans doute des algorithmes de 
conversion mais nous allons nous simplifier la tâche avec des fonctions internes de Python :relaxed:.


!!! info "Codage binaire"

    * La fonction `format(nb, '08b')` convertit le nombre `nb` en écriture binaire sur 8 bits. À noter que le type de sortie est `str`. Tester par exemple en console `format(65, '08b')` : on doit obtenir `'01000001'`.

     {{terminal()}}

     * Inversement, la fonction `int(chaine_bin, 2)` donne l'écriture décimale du nombre représenté par la chaine de caractères `chaine_bin` en base 2. Tester par exemple en console `int('01000001', 2)` : on doit obtenir 65.


On combinera ces deux fonctions pour coder en binaire chacun des caractères du message. Par exemple, `format(ord('A'), '08b')` fournit `'01000001'`. 

!!! question "Codage d'un message en écriture binaire"

    On veut pouvoir transformer un message en une succession de 0 et 1 obtenue en mettant bout à bout les codes ASCII (écrits en binaire) de chacun des caractères du message. 

    Par exemple, `"NSI"` doit donner "<font color="red">01001110</font> <font color="blue" >01010011</font> <font color="purple">01001001</font>" car :

    - `"N"` est représenté en ASCII par 78, c'est-à-dire <font color="red">01001110</font> en binaire,
    - `"S"` est représenté par 83, c'est-à-dire <font color="blue" >01010011</font>,
    -  et `"I"` est représenté par 73, c'est-à-dire <font color="purple">01001001</font>.

    Écrire le code de la fonction `message2bin`.

    {{IDE("message2bin")}}


!!! info "Manipulation de chaines de caractères par _slicing_"

    D'après le principe de la stéganographie exposé plus haut, on veut pouvoir modifier le dernier bit d'un code couleur. Pour cela, on va utiliser une technique de ***slicing*** (_hors-programme_, "découpage en tranches" spécifique à Python) :

    {{IDEv("slicing")}}

!!! info ""
    Cet outil de _slicing_ va aussi nous permettre de découper le message (écrit en binaire) en groupes de 3 bits :

    {{IDEv("slicing2")}}


!!! question "Modification des bits de poids faible d'un code RGB" 
    Dans l'exemple d'introduction, on a modifié le code RGB (72, 61, 139) d'un pixel pour y cacher `'110'` et ainsi obtenir le code RGB (73, 61, 138).

    Écrire le code de la fonction `modif_un_pixel(codeRGB, chaine3bits)` :

    {{IDE("modifPixel")}}


!!! question "Lecture des bits de poids faible d'un code RGB"

    Inversement, on veut pouvoir récupérer le texte `'110'` à partir du code RGB  (73, 61, 138). 

    Écrire le code de la fonction `decode_un_pixel(codeRGB)`, en remarquant par exemple que les bits de poids faible sont en réalité les bits de parité :

    {{IDE("decodePixel")}}


Enfin, pour faciliter le décodage, il sera préférable de coder dans l'image la longueur du message. Avec 15 bits, on peut coder tous les nombres inférieurs 2^15^ = 32768 et cela paraît suffisant comme longueur de texte. Comme nous utiliserons 3 bits par pixel (un pour chaque couleur), nous aurons donc besoin des 15÷3 = 5 premiers pixels pour coder la longueur du texte.


!!! question "Codage de la longueur du message" 
    Écrire le code de la fonction `codage_longueur` qui doit renvoyer l'écriture binaire sur 15 bits de la longueur du message.

    _Indication : puisque `format(nb, '08b')` donne l'écriture d'un nombre en binaire sur 8 bits, on comprend que `format(nb, '015b')` donne l'écriture sur 15 bits_. 

    {{IDE("codageLongueur")}}




## ^^3. Programmation d'un outil de stéganographie en Python^^

!!! info "En résumé"

    Nous avons réuni tous les outils pour cacher un message dans une image en modifiant les bits de poids faible du code RGB de chaque pixel, avec la convention suivante :

    - les 5 premiers pixels contiennent la longueur du message
    - les pixels suivants contiennent le message lui-même

    ![schema](schemaStegano.png){: .center}



Télécharger ces [fichiers compressés](FichiersStegano.zip), _à extraire dans votre répertoire de travail_.



### 3.1. Rappels de la manipulation d'images avec le module PIL

Ouvrir le fichier `Stegano_Rappels_PIL.py` dans un IDE (Thonny par exemple) pour un rappel des principales fonctions du module PIL : `getpixel`, `putpixel`, etc.

### 3.2. Codage par stéganographie

* Ouvrir le fichier `Stegano_Codage.py` et y copier les fonctions écrites sur cette page de cours à leur emplacement.
* Compléter le corps du programme, puis l'exécuter.
* Vérifier qu'il y a bien une nouvelle image dans dans votre répertoire.

!!! question "Challenge 1"

    Prendre l'image de votre choix et y cacher le message "La NSI c'est fantastique !" (ou tout autre message de votre choix). Transmettre ensuite l'image modifiée à votre professeur pour qu'il la décode.

### 3.3. Décodage

* Ouvrir le fichier `Stegano_Decodage.py` et y copier la fonction `decode_un_pixel` écrite sur cette page de cours.
* Compléter le corps du programme, puis l'exécuter.
* Verifier qu'un message est bien renvoyé en console.


!!! question "Challenge 2"

    Décoder le message contenu dans l'image `baboon_modif.png`.

!!! question "Challenge 3"

    Poster sur un réseau social une photo contenant un message codé par stéganographie, et inviter vos amis à découvrir le message !



### 3.4. Pour aller plus loin

Vous pourriez avoir envie de cacher une image dans une autre image ! Proposer une méthode pour réaliser ceci...

_Indice_ : il faut que la taille (en octets) de l'image à cacher soit au maximum 1/8 de la taille de l'image où l'on va la cacher. 

Vous pouvez commencer par traiter le cas où il faut cacher une image en noir et blanc (pas de couleur, pas de niveau de gris) comme par exemple l'image `img_3.png`. 

Ensuite vous pourrez généraliser à une image en couleur...
