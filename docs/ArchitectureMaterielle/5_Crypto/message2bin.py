def message2bin(message) :
    """
    Transforme message (une chaine de caractères) en une chaine de 0 et de 1 donnant le code binaire du message
    Entrée : message est une chaine de caractères
    Sortie : une chaine de caractère  (str) formée de 0 et de 1
    """
    pass # à remplacer par votre code


assert message2bin("NSI") == '010011100101001101001001', "problème de code"