def dechiffre_XOR(tab_crypte, clef) :
    """ Renvoie le texte obtenu par déchiffrement du tableau tab_crypté avec clef comme clé de  chiffrement
    Entrées :
        tab_crypte est un tableau d'entiers
        clef est de type str
    Sortie : une chaine de caractères
    """
    # compléter

assert dechiffre_XOR([27, 29, 105, 3, 22, 26, 29, 18, 14, 11, 115, 29, 28, 155, 26, 110, 0, 12, 13, 1, 12, 26], "NSI") \
       == "UN MESSAGE TRÈS SECRET"