# Sécurisation des communications & Cryptographie 🕵🏽‍♀️


## ^^1. Quelques définitions^^ 


!!! note "Coder, décoder"
    **Coder**, c'est représenter l'information par un ensemble de signes prédéfinis. **Décoder**, c'est interpréter un ensemble de signes pour en extraire l'information qu'ils représentent.

    Coder et décoder s'emploient lorsqu'il n'y a pas de secret. Par exemple on peut coder/décoder des entiers relatifs par une suite de bits par un «codage en complément à deux».

!!! note "Cryptographie"
    La **cryptographie** est une discipline veillant à protéger des messages (pour en assurer la confidentialité, l'authenticité et l'intégrité), par l'intermédiaire de clés de chiffrements.

    La cryptographie est utilisée depuis au moins l'antiquité.

    La **cryptanalyse** désigne les techniques visant à déduire un texte en clair à  partir d'un texte chiffré sans posséder la clé de chiffrement. Le processus par lequel on tente de comprendre un message en particulier est appelé une attaque.

    Chiffrer un message, c'est rendre une suite de symboles incompréhensible au moyen d'une clé de chiffrement.

    Déchiffrer ou décrypter, c'est retrouver la suite de symboles originale à partir du message chiffré. On utilise _déchiffrer_ quand on utilise la clé de chiffrement pour récupérer le texte original, et _décrypter_ lorsqu'on arrive à retrouver le message original sans connaitre la clé de chiffrement.



## ^^2. Chiffrement symétrique^^ 

!!! note ""
    On parle de cryptographie **symétrique** lorsque la même clé est utilisée pour chiffrer et déchiffrer un message.


### 2.1. Code de César, code de Vigenère.

!!! note "Code de César"
    Le chiffre de César est une méthode de chiffrement très simple utilisée par Jules César dans ses correspondances secrètes (ce qui explique le nom « chiffre de César »).

    Le texte chiffré est obtenu en remplaçant chaque lettre du texte original par une lettre obtenue par un décalage de l'alphabet. La longueur du décalage (3 dans l'illustration ci-dessous) constitue la clé de chiffrement.

    ![](codeCesar.png){: .center width 30%}

    Puisqu'il n'y a que 26 décalages possibles, il est très facile par une attaque "force brute" de décrypter un message chiffré avec un code de César.

!!! note "Code de Vigenère"
    Étant donné un message, par exemple `ENIGME`, et une clé de chiffrement, par exemple `NSI`, on recopie plusieurs fois la clé sous le message :

        ENIGME
        NSINSI
    
    Chaque caractère du message est associé à une valeur numérique entière, par exemple sa position dans l'alphabet :
    
        04 13 08 06 12 04
        13 18 08 13 18 08

    On additionne ensuite les nombres et, si ça dépasse 26, on enlève 26.

    Dans notre exemple, on obtient donc

        17 05 16 19 04 12

    On remplace enfin chaque nombre par la lettre correspondante de l'alphabet.

        R F Q T E M

    On voit plusieurs améliorations par rapport au code de César : 
    
    * le nombre de clés de chiffrement est presque infini et plus la clé est longue, plus le décryptage est compliqué ;
    * une lettre (le E du début et le E de la fin par exemple) n'est pas toujours chiffrée de la même façon.


!!! question "Exercice"

    1. Chiffrer le message `VOLTAIRE` avec la clé `LYC`.
    2. Déchiffrer le message `LRVLOWP` avec la clé `LYC`.

    On vous donne les correspondances lettre/position dans l'alphabet :

    | A | B | C | D | E | F | G | H | I | J | K | L | M |
    | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---:|:---: | :---: | :---: | :---: |
    | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 |

    | N | O | P | Q | R | S | T | U | V | W | X | Y | Z |
    | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---:|:---: | :---: | :---: | :---: |
    | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 |


### 2.2. Chiffrement XOR

#### 2.2.1. Principe du chiffrement XOR

L'opérateur binaire `XOR` correspond au **"OU eXclusif"** : `a XOR b` vaut `True` si et seulement si un seul des booléens `a` et `b` vaut `True`.

Sa table de vérité est donc :

| a | b | a XOR b |
| :---: | :---: | :---: |
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 0 |

On remarquera que `XOR` est _réversible_, dans le sens où :
    si `c = a XOR b`, alors `a = c XOR b`.

!!! note "Chiffrement XOR"

    Le principe du chiffrement XOR est très similaire à celui du chiffrement de Vigénère mais il est plus adapté à l'univers numérique car il repose sur l'opérateur binaire `XOR`.
    
    Étant donné un message, par exemple «UN MESSAGE TRÈS SECRET», et une clé de chiffrement, par exemple «NSI», on recopie plusieurs fois la clé sous le message :

        UN MESSAGE TRÈS SECRET
        NSINSINSINSINSINSINSIN

    Chaque caractère du message est associé à une valeur numérique entière (par exemple son code ASCII/UTF8) :

        85 78 32 77 69 83 83 65 71 69 32 84 82 200 83 32 83 69 67 82 69 84
        78 83 73 78 83 73 78 83 73 78 83 73 78  83 73 78 83 73 78 83 73 78

    On effectue ensuite l'opération `XOR` entre chaque bit du nombre du message et de la clé.
    
    Par exemple pour le premier caractère :

    $$\begin{array}{cccccccccr}
    &0&1&0&1&0&1&0&1&&85\\
    \textrm{XOR}&0&1&0&0&1&1&1&0&&78\\\hline
    &0&0&0&1&1&0&1&1&&27
    \end{array}$$

    Le code obtenu dans notre exemple est donc :

        27 29 105 3 22 26 29 18 14 11 115 29 28 155 26 110 0 12 13 1 12 26

    L'opérateur `XOR` étant réversible, il suffira de refaire un `XOR`  sur le message chiffré pour retrouver le message original.


!!! question "Exercice"
    1. Chiffrer le message `ALAN` avec la clé `NSI`.
    2. Déchiffrer le message `[20, 6, 29]` avec la clé `NSI`.

    On donne le tableau de correspondance entre une lettre et son code ASCII écrit en décimal et en binaire.

    ![](tab_ascii.png){: .center width 40%}


#### 2.2.2. Codage en Python du chiffrement XOR

Pour coder en Python le chiffrement/déchiffrement XOR, on pourra utiliser les fonctions `ord` et `chr`, ainsi que le symbole `^` qui correspond à l'opérateur `XOR` :

```pycon
>>> ord('A')
65
>>> chr(65)
'A'
>>> 85 ^ 78
27
```


Compléter les fonctions ci-dessous.

{{ IDE("get_utf8")}}

{{ IDE("get_string")}}

Puisque la clé est potentiellement plus petite que le message à chiffrer, on pourrait créer un code pour la rallonger à la longueur du message. Mais on peut éviter ceci en remarquant que 

    position de la lettre de la clé = 
    reste dans la division de la position de la lettre du message par la longueur de la clé.

Compléter alors les codes ci-dessous.

{{ IDE("chiffre_xor")}}

{{ IDE("dechiffre_xor")}}

## ^^3. Chiffrement asymétrique^^

Le gros problème avec le chiffrement symétrique, c'est qu'il faut que les deux personnes se mettent d'accord sur la clé qui sera utilisée lors des échanges : cette communication de la clé entre les deux personnes peut représenter une **faille de sécurité**. Le chiffrement asymétrique permet d'éviter ce problème.

Le principe d'un **chiffrement asymétrique** est l'existence d'une **clé publique**, que son propriétaire met à la disposition de tout le monde, et d'une **clé privée**, que son propriétaire conserve uniquement pour lui. 

![asym](asym.png){: .center}

Le chiffrement asymétrique actuellement le plus répandu est le chiffrement RSA, mis au point en 1977 par trois chercheurs du MIT (USA) : Ron Rivest, Adi Shamir et Len Adleman. 

??? note "Quelques explications sur le chiffrement RSA"
    RSA se base sur l'utilisation de très grands nombres premiers. Si vous prenez un nombre premier A (par exemple A = 16813007) et un nombre premier B (par exemple B = 258027589), c'est très rapide de déterminer le produit C de A par B (ici C = A x B = 4338219660050123). En revanche, si on vous donne C (ici 4338219660050123), il est très long de retrouver A et B : il faut tester un par un tous les nombres premiers pour voir le(s)quel(s) est un diviseur de C. À ce jour, aucun algorithme n'est capable de retrouver A et B connaissant C dans un temps "raisonnable". 
    
    Les nombres premiers A et B utilisés sont en pratique très grands (au moins 300 chiffres dans leur écriture décimale !). Les détails du fonctionnement de RSA sont relativement complexes (mathématiquement parlant) et ne seront pas abordés ici. Vous devez juste savoir qu'il existe un lien entre une clé publique et la clé privée correspondante, mais qu'il est quasiment impossible de trouver la clé privée de quelqu'un à partir de sa clé publique.

Pour plus de détails sur le chiffrement asymétrique (puzzles de Merkle, méthode de Diffie-Hellman, RSA) : voir le cours de ce [site](https://www.zonensi.fr/NSI/Terminale/C11/Securisation/#cryptographie-asymetrique).

## ^^4. TP Communication par chiffrement asymétrique^^

### 4.1. Préparation des clés

Nous allons utiliser l'outil `gpg` (_GNU Privacy Guard_) qui implémente entre autres le chiffrement RSA. Plus d'information [ici](https://fr.wikibooks.org/wiki/GPG).

**a)** Sur un PC sous Linux, ouvrir un terminal puis saisir la commande qui va générer vos clés publique et privée :

    gpg --full-generate-key

Cette commande est interactive et nécessite de répondre aux questions suivantes :

- choix du chiffrement : prendre le choix (1) RSA
- longueur de la clé : le standard est 2048
- durée de validité : saisir `1w` pour une durée de 1 semaine, et valider avec `o`
- identité : entrer un nom ou un pseudo puis une adresse mail quelconque, même fictive ; ne rien mettre en commentaire
- saisir `O` pour valider
- mot de passe : laisser vide ou bien remettre votre nom ou pseudo (à bien retenir alors !), puis valider.

Dans le répertoire `/home/eleve/.gnupg/`, vous trouverez alors un fichier `pubring.kbx` qui est votre clé publique. La clé privée est dans le sous-répertoire `private-keys-v1.d`.


**b)** Diffusion de la clé publique

* On commence par mettre la clé publique sous forme d'un fichier texte. Pour cela, saisir la commande suivante dans un terminal :

    `gpg --export --armor votre_nom_ou_pseudo > ~/clé_publique_prénom.asc`

    en remplaçant évidemment `votre_nom_ou_pseudo` par le nom donné dans la phase de création de la clé et `prénom` par votre propre nom et prénom.

* Dans le répertoire `/home/eleve/`, vous trouverez alors le fichier `clé_publique_prénom.asc`, que vous pouvez ouvrir avec un éditeur de texte.

* Déposer ensuite le contenu de ce fichier `.asc` dans le "Forum des clés publiques" de Moodle, en faisant un `Répondre` à l'un des messages.


### 4.2. Envoi d'un message chiffré

Supposons que vous vouliez envoyer un message à Alice. 

* Dans "Forum des clés publiques" de Moodle, repérer le contenu du fichier `clé_publique_Alice.asc` et le copier/coller sur votre ordinateur à l'aide d'un éditeur de texte.

* Dans un terminal, ajouter cette clé à votre "trousseau de clés connues" avec la commande 

    `gpg --import clé_publique_Alice.asc`

    Dans les lignes qui s'affichent, bien ^^repérer l'identité^^ (le nom ou pseudo) choisi par Alice : dans la suite de ce tuto, on fera comme si c'était `AliceCarroll`.

* Dans un éditeur de texte, écrire votre message pour Alice et enregistrer le fichier au format `txt`, par exemple `pourAlice.txt`.

* Dans un terminal, chiffrer ce fichier avec la commande 

    `gpg --recipient AliceCarroll --armor --encrypt pourAlice.txt`

    Ceci génère un fichier `pourAlice.txt.asc`, lisible par un éditeur de texte mais incompréhensible.

* Dans le "Forum des messages chiffrés" de Moodle, faire un `Répondre` à l'un des messages pour entrer le vôtre : indiquer d'abord «Pour Alice» puis copier/coller le contenu du fichier `pourAlice.txt.asc`.

Tout le monde sait donc que vous envoyez un message à Alice, mais seule Alice sera capable de déchiffrer le message !

### 4.3. Déchiffrement du message

Alice constate dans le "Forum" qu'on lui a envoyé un message.

* Copier/coller la partie incompréhensible du message dans un éditeur de texte et sauvegarder le fichier en lui donnant un nom, par exemple `monMessage.txt.asc`

* Dans un terminal, déchiffrer le message avec la commande 

        gpg --decrypt monMessage.txt.asc > clair.txt

Ceci génère un fichier `clair.txt` contenant le message en clair envoyé à Alice. Et voilà :sunglasses: !



