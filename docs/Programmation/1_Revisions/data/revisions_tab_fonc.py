# cas d'un nombre
nombre = 3
def f1(nombre) :
    nombre = nombre + 4
    print("dans la fonction, le nombre vaut ", nombre)
    return None

f1(nombre) # appel de la fonction f1 pour y passer
print("après passage dans la fonction, le nombre vaut", nombre)

# cas d'un tableau
tab = [1,2,3]
def f2(tab) :
    tab.append(4)
    print("dans la fonction, le tableau vaut ", tab)
    return None

f2(tab) # appel de la fonction f2 pour y passer
print("après passage dans la fonction, le tableau vaut ", tab)
