
# Révisions & Compléments :snake:

!!! tip "Mémento Python"   
    À avoir sous la main en cas de trou de mémoire : le [mémento Python](data/Memento_NSI.pdf)

## ^^1. Opérateurs arithmétiques^^

Le code suivant n'affiche pas de bétises...

{{ IDEv("data/revisions_operateur")}}

... mais le test ` if k/2 == k//2` semble bien compliqué. 

Par quel test beaucoup plus simple pouvez-vous le remplacer ?

??? danger "Solution"

    On peut utiliser le test suivant : `if k%2 == 0`

## ^^2. Fonctions^^

### 2.1. Portée d'une variable : variable locale, variable globale.


!!! tip "Définitions :heart:"
    - Les variables définies dans le corps d'une fonction sont appelées **variables locales**.
    - Les variables définies dans le corps du programme (_sous-entendu_ : pas à l'intérieur d'une fonction) sont appelées **variables globales**.


On dit que les fonctions créent leur «espace de noms» (*espace* est à prendre au sens d'*univers*, de *monde parallèle*), un espace qui leur est propre.

Quelles sont les règles régissant ces espaces de noms ? Les frontières entre ces espaces sont elles poreuses ? 


!!! tip "Règles d'accès aux variables locales et globales :heart:"
    - **règle 1 :** une **variable locale** (définie au cœur d'une fonction) est **inaccessible** hors de cette fonction.
    - **règle 2 :** une **variable globale** (définie à l'extérieur d'une fonction) est **accessible** en **lecture** à l'intérieur d'une fonction.
    - **règle 3 :** une **variable globale** (définie à l'extérieur d'une fonction) **ne doit pas être modifiée** à l'intérieur d'une fonction.


![image](data/global_regles.png){: .center width=80%}


!!! question "Exercice"
    === "Énoncé"
        On considère les 3 codes ci-dessous. Pour chacun, dire **sans l'exécuter** s'il est valide ou non. S'il ne l'est pas, identifier la règle (parmi celles énoncées ci-dessus) qui est bafouée.

        **code A**
        ```python linenums='1'
        points = 0
        def verdict(reponse):
            if reponse > 10:
                points += 3

        verdict(12)
        ```    

        **code B**
        ```python linenums='1'
        def bouge(x):
            decalage = 25
            x += decalage
            return x

        y = bouge(100)
        print(decalage)
        ```

        **code C**
        ```python linenums='1'
        def test_bac(moyenne):
            if moyenne >= 10:
                print("admis !")

        def coup_de_pouce(note):
            return note + bonus

        bonus = 0.6
        ma_moyenne = 9.5
        ma_moyenne = coup_de_pouce(ma_moyenne)
        test_bac(ma_moyenne)
        ```

    === "Correction code A"
        Ce code n'est pas valide, car il contrevient à la règle 3.

        ```ligne 4``` : la modification de la variable globale ```points``` est interdite.

    === "Correction code B"
        Ce code n'est pas valide, car il contrevient à la règle 1.

        ```ligne 5``` : l'accès à la variable locale ```decalage``` en dehors de la fonction `bouge`  est interdit.

    === "Correction code C"
        Ce code est valide.

        ```ligne 6``` : l'accès à la variable globale ```bonus``` est autorisé, selon la règle 2.            



!!! info "Paramètre d'une fonction = variable locale"
    Quand on définit une fonction avec un paramètre, par exemple `maFonction(monNombre)`, le paramètre (ici `monNombre`) se comporte comme une variable locale.

    Il est notamment impossible d'y accéder en dehors de la fonction.

    Par exemple, le code suivant génère une erreur :

    {{ IDE("data/fonction_bug")}}



Un programmeur peu attentif peut créer une variable locale dans une fonction qui porte le même nom qu'une variable globale. Dans cette situation, la variable locale **masque** la variable globale : c'est la variable qui "prend le dessus" tant que l'on est dans la fonction.

!!! question "Exercice"
    Dans le code ci-dessous, le programmeur peu attentif a utilisé le mot `message` pour créer une variable locale et une variable globale.

    _Merci de répondre à la question avant d'appuyer sur le bouton Lancer_ :wink:

    {{ IDE("data/fonction_masquage")}} 

    {{ multi_qcm(
        [
            "Que va-t-on obtenir à l'affichage après exécution du code ?",
            ["'Bienvenue Alice' puis 'Bonjour Alice'","'Bienvenue Alice' deux fois de suite","'Bonjour Alice' deux fois de suite"],[1],
        ],
        )}}


!!! tip "Une bonne pratique"
    Pour éviter les erreurs, une bonne pratique est de ne pas utiliser le même nom pour une variable locale et une variable globale.

    Mais ce n'est qu'une bonne pratique, ce n'est pas une obligation : à vos risques et périls !


!!! question "Exercice "

    On considère le code suivant :

    ```python
    def ma_fonction(x):
        x = x + 1
        return 2*x
    # corps du programme
    y = ma_fonction(1)
    print("la variable x vaut ",x, " et la variable y vaut ",y)
    ```

    {{ multi_qcm(
        [
            "Qu'obtient-on à l'affichage après exécution de ce code ?",
            ["la variable x vaut 1 et la variable y vaut 2", 
            "la variable x vaut 1 et la variable y vaut 4",
            "la variable x vaut 2 et la variable y vaut 4",
            "on a un message d'erreur `#!py NameError, name 'x' is not defined`"],[4],
        ],
        shuffle = False)}}

    ??? danger "Explication"
        Pour bien visualiser : copier et exécuter le code dans [Python Tutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"}

        Quand un programme exécute une fonction, c'est un peu comme s'il partait dans un monde parallèle. Ici, la variable `x` n'existe que dans la fonction : c'est une variable **locale**.
        Elle n'existe plus quand on revient dans le corps du programme.



!!! question "Exercice "
    
    On considère le code suivant :

    ```python
    def ma_fonction(x):
        x = x + 3
        return 2*x
    # corps du programme
    x = 1
    y = ma_fonction(x)
    ```

    {{ multi_qcm(
        [
            "Quelles sont les valeurs des variables `#!py x` et `#!py y` après exécution du code suivant ?",
            ["la variable x vaut 4 et la variable y vaut 8",
             "la variable x vaut 1 et la variable y vaut 8",
             "la variable x vaut 1 et la variable y vaut 2",
            "on a un message d'erreur `#!py NameError, name 'y' is not defined`"],[2],
        ],
        shuffle = False)}}

    ??? danger "Explication"
        Pour bien visualiser : copier et exécuter le code dans [Python Tutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"}
        
        La variable `x` créée dans le corps du programme et qui vaut 1 est une variable **globale**. Cette variable globale n'est pas modifiée dans la suite du corps de programme : elle reste donc à 1 jusqu'à la fin. 

        Par contre, la fonction `ma_fonction` est définie à l'aide d'un paramètre qui s'appelle aussi `x`, mais ce `x` ne "vit" qu'à l'intérieur de la fonction : c'est une **variable locale** qui **masque** le `x` global. Quand le programme "entre" dans la fonction, le `x` local vaut au départ 1, puis il passe à 4 avec l'instruction `x = x + 3`. La fonction renvoie alors la valeur 2×4 = 8, qui est "capturée" par la variable `y`. 

        > Remarque : le code de l'exemple 2 n'est donné qu'à but pédagogique, pour faire percevoir la distinction entre variable globale et variable locale. Mais ce n'est pas un code très lisible et on évitera d'écrire des programmes avec de telles ambigüités.


!!! warning "Exception à la règle n°3"
    Il y a quand même une exception à la règle n°3 qui stipule qu'on ne doit pas modifier une variable globale dans une fonction : c'est quand cette variable est un tableau/une liste.

    En effet, la manipulation des valeurs du tableau et l'utilisation de méthodes de listes comme `append` conduisent à une **modification "sur place"** de la liste. 
    
    Dans le code ci-dessous, la variable globale `monTableau` a été modifiée après son passage dans la fonction !

    {{IDE("data/revisions_modif_inplace")}}

### 2.2. Fonctions imbriquées

On peut, à l'intérieur d'une fonction, appeler une autre fonction.


!!! Question " QCM "

    {{ multi_qcm(
        [
            """
            On exécute le script ci-dessous.
 
            ```python
            def f(x):
                z = 2*x
                return z
            
            def g(x):
                x = x+1
                return f(x)
            # corps du programme    
            x = 4
            y = g(x)

            ```

            Quelle est la valeur de la variable `y` après exécution du code ?
            """,
            ["la variable y vaut 5", "la variable y vaut 8", "la variable y vaut 10", "on a un message d'erreur `#!py NameError, name 'f' is not defined`"],[3],
        ],
         shuffle = False)}}

    ??? danger "Explication"
        Pour bien visualiser : copier et exécuter le code dans [Python Tutor](https://pythontutor.com/visualize.html#mode=edit){target="_blank"}
        
        On a ici un exemple de ***fonctions imbriquées*** : la fonction `f` est appelée à l'intérieur de la fonction `g`. 
        
        À chaque appel de fonction, on part dans un nouveau "monde parallèle", avec ses propres variables locales.

    
### 2.3. Une nouveauté : valeur par défaut d'un argument d'une fonction

Python permet aux arguments de fonction d’avoir des **valeurs par défaut**. Si la fonction est appelée sans l’argument, l’argument obtient sa valeur par défaut.

{{ IDEv("data/revisions_vardefaut")}}


> En réalité, vous utilisez depuis déjà longtemps des valeurs par défaut. Quand vous écrivez `for k in range(10)`, vous utilisez les valeurs par défaut de `range(debut,fin,incrément)`, qui sont `debut = 0` et `increment = 1`.


!!! question "QCM  "

    {{ multi_qcm(
        [
            """
            Quelle est la valeur de la variable `message` après exécution du code ci-dessous ?

            ```python
            def bonjour(prenom = 'Alice') :
                txt = 'Bonjour ' + prenom
                return txt

            message = bonjour()
            ```
            """,
            ["la variable  `message` vaut 'Bonjour Alice'", "la variable  `message` vaut 'Bonjour prenom'", "la variable `message` vaut 'Bonjour'", "on a un message d'erreur `TypeError, missing 1 required positional argument`"],[1],
        ],
        shuffle = False)}}

## ^^3. Les tableaux (ou _listes_)^^

### 3.1. Création d'un tableau

Supposons que l'on souhaite créer un tableau `tab_puissance` contenant les 10 premières puissances de 2 (de 2^0^ jusqu'à 2^9^). On dispose en Python de plusieurs possibilités :

* initialisation d'un tableau rempli d'une valeur quelconque puis modification de ces valeurs :

{{ IDE("data/revisions_tab1")}}

> _Remarque_ : l'intruction `[0] * 10` crée un tableau de 10 cases remplies de 0. L'instruction `[1,2,3] * 2` crée le tableau `[1,2,3,1,2,3]`. 

* création par _compréhension_ :

{{ IDE("data/revisions_tab2")}}

* création d'un tableau vide puis remplissage par la _méthode_ `append` :

{{ IDE("data/revisions_tab3")}}

> Remarque :  en ligne 3, au lieu de l'instruction `tab_puissance.append(2**k)` qui ajoute la valeur `2**k` à la fin du tableau `tab_puissance`, on aurait pu effectuer l'opération de _concaténation_ `tab_puissance = tab_puissance + [2**k]`.


!!! question "QCM : création d'un tableau" 

    {{ multi_qcm(
        [
            """
            On veut créer la variable `tab_impairs = [9,7,5,3,1]`.
    
            On propose quatre codes, mais seuls trois d'entre eux produisent le résultat attendu. 

            ```python
            # Code 1
            tab_impairs = []
            for k in range(9,-1,-2) :
                tab_impairs.append(k)

            # Code 2
            tab_impairs = []
            k = 9
            while k >= 1 :
                tab_impairs.append(k)
                k = k - 2

            # Code 3
            tab_impairs = [k for k in range(9,1,-1) if k%2 == 1]

            # Code 4
            tab = [k for k in range(1,10,2)]
            tab_impairs = [0] * len(tab)
            for i in range(len(tab)):
                tab_impairs[i] = tab[len(tab)-1 -i]
            ```

            Quel code ne fournit pas le résultat attendu ?
            """,
            ["Code 1", "Code 2", "Code 3", "Code 4"],[3],
        ],
        shuffle = False)}}

    ??? danger "Explication"
        La variable `k` créée par l'instruction `for k in range(9,1,-1)` démarre à 9 puis diminue de 1 à chaque tour de boucle : elle passe à 8, puis 7, etc. et s'arrête à 2 car la valeur `end=1` n'est **jamais** atteinte. Avec le code 3, le tableau `tab_impairs` vaut alors `[9,7,5,3]`.  



### 3.2. Parcours séquentiel d'un tableau  : les trois algorithmes usuels

!!! tip "Algorithmes classiques :heart:" 
    Les trois algorithmes classiques de :

    * calcul de la moyenne d'un tableau de nombres ;
    * calcul du minimum ou du maximum des valeurs d'un tableau de nombres ;
    * recherche d'une valeur dans un tableau avec renvoi de `True` si la valeur est trouvée ou `False` sinon

    doivent être connus par cœur !

    Revoir si besoin le [cours de Première](https://pasallard.gitlab.io/premiere_nsi_voltaire/Algorithmique/Algo_Intro/Algo_Intro/#5-algorithmes-lies-au-parcours-sequentiel-dun-tableau){target="_blank"}.

!!! question "Saurez-vous reconnaître cet algorithme ?"
    Voici le code d'une fonction "mystère" : 
    ```python
    def mystere(tab,val):
        k = 0
        flag = tab[k] == val
        while not(flag) and ( k+1 < len(tab) ) :
            k = k+1
            flag = tab[k] == val
        return flag
    ```

    À quoi sert cette fonction ? Écrire une fonction plus simple qui fournit le même résultat que cette fonction `mystere`.

    {{ IDE()}}

??? danger "Solution"
    Pour se mettre sur la piste, on peut faire quelques appels de fonction comme par exemple :

    ```python
    print(mystere([1,2,3,4,5],5))
    print(mystere([1,2,3,4,5],1))
    print(mystere([1,2,3,4,5],8))
    ```

    On se rend alors compte cette fonction renvoie `True` si `val` appartient à `tab` et `False` sinon. Le code plus simple est donc celui de l'algorithme de recherche vu en classe de Première.


### 3.3. Fonctions natives de Python, appartenance à un tableau

!!! warning "Fonctions `sum`, `min` et `max`"
    Python dispose de fonctions *natives* `sum`, `min` et `max` que l'on peut appliquer à un tableau.

    Par exemple, l'instruction `total = sum( [1,2,3] )` affecte la valeur 6 à la variable `total`.

    Ces fonctions natives peuvent être employées dans une Épreuve Pratique si ce sont **juste des étapes de calculs intermédiaires** de votre code, et non la partie principale.

    Notamment, si on vous demande d'écrire l'algorithme de calcul du maximum d'un tableau, on attend que vous écriviez l'algorithme vu en Première et non pas simplement :

    ```python
    def maximum (tab) :
        return max(tab)
    ```


!!! warning "Appartenance à un tableau" 
    Une expression du type `val in tab` permet 
    
    - soit la création d'un booléen qui vaut `True` si la variable `val` appartient au tableau `tab` ;
    - soit de faire tourner une boucle `for val in tab` où la variable `val` va parcourir tout le tableau `tab`. 
    
    Ceci peut être employé dans une Épreuve Pratique si c'est **juste une étape intermédiaire** de votre code, et non la partie principale.

    Par exemple, le code ci-dessous n'est pas acceptable comme solution au problème "écrire le code de recherche d'une valeur dans un tableau" (on attend là encore l'algorithme vu en Première).

    ```python
    def recherche(tab, val):
        return val in tab
    ```

!!! question "QCM"

    {{ multi_qcm(
        [
            """
            Voici le code d'une fonction `bidule` :

            ```python
            def bidule(tab,val):
                mon_tab = [elt for elt in tab if elt == val]
                n = len(mon_tab)
                return n
            ```

            Quelle valeur est renvoyée par l'appel `bidule(['B','A','R','B','A','R','O','S','S','A'],'A')` ?
            """,
            ["3","10", "['A', 'A', 'A']","7"],[1],
        ])}}

    ??? danger "Explication"
        Il y a 3 fois la lettre 'A' dans le tableau `tab`.
        Le code en question correspond à un calcul du **nombre d'occurrences** de la valeur `val` dans le tableau `tab`.

### 3.4. Listes et fonctions

On rappelle (voir la partie [Portée d'une variable](#21-portée-dune-variable--variable-locale-variable-globale)) une particularité des listes : contrairement aux variables numériques (type `int` ou `float`) ou alphanumériques (type `str`), elles sont **modifiées en l'état par passage dans une fonction** si on touche aux valeurs de leurs éléments ou qu'on leur applique une méthode comme `append`.

Un exemple est plus parlant :

{{ IDE("data/revisions_tab_fonc")}}


### 3.5. Une nouveauté : la méthode `pop`

!!! tip "Enlever et récupérer une valeur d'un tableau avec la méthode `pop`"
    Étant donné un tableau `tab`, l'instruction `v = tab.pop(0)` a deux effets :

    - elle supprime la valeur d'indice 0 du tableau `tab`
    - et elle transmet cette valeur supprimée à la variable `v`.

    Si c'est la valeur d'indice 1 (celle en 2^e^ position du tableau) que l'on veut enlever et récupérer, on écrira `v = tab.pop(1)`. Et ainsi de suite.

    {{ IDE("data/revisions_pop")}}

    

!!! question "QCM : enlever et récupérer la dernière valeur d'un tableau"

    {{ multi_qcm(
        [
            "Étant donné un tableau `tab`, quelle instruction permet d'enlever la dernière valeur de ce tableau et d'affecter à une variable `w` cette valeur enlevée ?",
            ["`w = tab[len(tab)]`", "`w = tab.pop(last(tab))`", "`w = last(tab)`", "`w = tab.pop([len(tab)-1])`"],[4],
        ],
        shuffle = False)}}

    ??? danger "Explication"
        La dernière valeur d'un tableau `tab` a pour rang `len(tab) -1` (par exemple : un tableau de **10** valeurs a des cases numérotées de 0 à **10 -1** = 9). On met donc ce rang `len(tab) -1` comme argument de la méthode `pop`.


### 3.6. Une autre nouveauté : la méthode `index`

!!! tip "Obtenir l'indice d'une valeur dans un tableau avec la méthode `index`"
    Étant donné un tableau `tab` et une valeur `val` appartenant au tableau, l'instruction `r = tab.index(val)` affecte à la variable `r` l'indice (le rang, le "numéro de case") de la valeur `val` dans le tableau `tab`.

    Si la valeur `val` n'est pas présente dans le tableau, une erreur `ValueError` interrompt le programme.

    {{ IDE("data/revisions_index")}}


## ^^4. Chaines de caractères^^

En Python, il y a beaucoup de points communs entre la manipulation des chaines de caractères et celles des tableaux.

Par exemple, la série d'instructions ci-dessous  est licite :
```python
mot = "info"        # affectation de la valeur "info" à la variable mot
mot1 = mot * 3      # mot1 vaut 'infoinfoinfo'
mot2 = mot + 'rmatique'    # mot2 vaut 'informatique'
var = mot[0]        # var est la chaine de caractère "i"
n = len(mot)        # n vaut 4
bool = 'z' in mot    # bool vaut False 
```

:warning: Attention : les _méthodes_ `append` et `pop` sont spécifiques aux tableaux et **ne s'appliquent pas** aux chaines de caractères !

??? QCM "Dernière lettre d'une chaine de caractères"

    {{ multi_qcm(
        [
            " Quelle instruction permet d'affecter à la variable `var` la dernière lettre d'une chaine de caractères `mot` ?",
            ["`var = mot[len(mot)]`", "`var = mot[len(mot)-1]`", "`var = last(tab)`", " `var = mot.pop(len(mot)-1)`"],[2],
        ],
        shuffle = False)}}



??? info "Une méthode spécifique aux chaines de caractères"
    Étant donné une chaine de caractères composée de plusieurs mots, il peut être utile de récupérer ces mots individuellement : cela se fait avec la _méthode_ `split` (_séparer_ en anglais). 
    Tester par exemple le code :

    ```python
    monTexte = "je vais bien"
    tab = monTexte.split()
    print(tab)
    ```

    {{ terminal()}}



## 5. ^^Les ***tuples***^^

Par commodité, on gardera l'appelation anglo-saxonne _tuple_, plus courte que le "p-uplet" français.

### 5.1. Les tuples ne sont pas ***mutables***

À la différence des tableaux, on ne peut pas modifier un élément (_une case_) d'un _tuple_ déjà existant. Tester par exemple le code suivant :

{{ IDE("data/revisions_tuple1", MAX=3)}}

Si on veut modifier un _tuple_, on est obligé de le redéfinir complètement. Par exemple, pour ne pas avoir d'erreur dans l'exécution du code ci-dessus, il faut écrire `Triplet=(4,777,6)` à la place de la ligne 3. 

### 5.2. Tuples et fonctions

Comme les tableaux ou les chaines de caractères, les _tuples_ peuvent être utilisés dans des fonctions.



{{ multi_qcm(
    [
        """
        On exécute le code suivant :

        ```python
        def truc(C):
            # C est un couple (tuple) de deux nombres
            if C[0] < C[1] :
                R = (C[0],C[1])
            else :
                R = (C[1],C[0])
            return R

        t = truc( (58,31))
        ```

        Quelle est la valeur de la variable `t` ?
        """,
        ["(31,58)", "58", "(1,0)", "31"],[1],
    ],
    qcm_title = "QCM"
)}}

## ^^6. Exercices et TP^^

* Généralités : télécharger le [carnet Jupyter](Prog_RevisionsComplements.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}

* Exercice type Bac n°1 :

    * [énoncé](Prog_Python_SujetBac_JA2.pdf), à faire d'abord sur papier ;
    * [codage sur un carnet Jupyter](Prog_Python_SujetBac_JA2.ipynb), pour tester vos réponses.

* Exercice type Bac n°2 : [énoncé](Prog_Python_SujetBac_2022J2ME3.pdf).

* Exercice type Bac n°3 :

    * [énoncé](Prog_Python_SujetBac_2022J2AN1.pdf), à faire d'abord sur papier ;
    * [codage sur un carnet Jupyter](Prog_Python_SujetBac_2022J2AN1.ipynb), pour tester vos réponses et visualiser le résultat.

* TP sur le Jeu de la Vie : 

    * télécharger le [carnet Jupyter](TP_JeuVie.ipynb)
    * pour traiter la question A2 et jouer au jeu de la Vie dans un navigateur Web : télécharger le [dossier zippé](JeuVie.zip).


* _(Pour les rapides)_ Exercice type Bac n°4 : [énoncé](Prog_Python_SujetBac_2021J2ME2.pdf).