from random import randint
from random import choice

from Bulles import * # il est important que votre fichier se nomme Bulles.py
 
        
## gestion du jeu

def detect_collision(Mousse) :
    for i in range(len(Mousse)-1) :
        for j in range(i+1,len(Mousse)) :
            if Mousse[i] is not None and Mousse[j] is not None and bullesEnContact(Mousse[i],Mousse[j]) :
                if Mousse[i].rayon < Mousse[j].rayon :
                    collision(i,j,Mousse)
                else : 
                    collision(j,i,Mousse)

def detect_bord_rebond(Mousse) :
    for bul in Mousse :
        if bul is not None :
            if (bul.xc - bul.rayon <= 0 and bul.dirx < 0) or (bul.xc + bul.rayon >= 100 and bul.dirx > 0) : # bords droit/gauche
                bul.dirx = - bul.dirx # chgt de direction latérale
            if (bul.yc - bul.rayon <= 0 and bul.diry < 0) or (bul.yc + bul.rayon >= 100 and bul.diry > 0) : # bords bas/haut
                bul.diry = - bul.diry # chgt de direction verticale


## interface graphique
from tkinter import *
import time

fenetre = Tk()

fenetre.title('Bulles')
fenetre['bg']='white'

largeur=800
hauteur=largeur
can=Canvas(height=hauteur,width=largeur)
can.pack()

speed_animation = 1 # plus le nombre est élevé, moins ça va vite
grossissement = largeur//100

def couleurBulle(codeCouleur) : 
    """ codeCouleur est un nombre entre 0 et 65536 = 256*256 et il faut générer en sortie en code RVB"""
    tabRVB = [hex(codeCouleur%256),hex(codeCouleur//256), hex((codeCouleur*3)%256) ]  # choix perso
    newTxt = "#"
    for txtCoul in tabRVB :
        for k in range(2, len(txtCoul)): # 
            newTxt = newTxt + txtCoul[k]
        if len(txtCoul) < 4 :
            newTxt = newTxt + '0'
    return newTxt

def animation() :
    can.delete("all")
    for bul in Mousse :
        if bul is not None :
            txtCouleur = couleurBulle(bul.couleur)
            can.create_oval(grossissement*(bul.xc-bul.rayon),grossissement*(bul.yc-bul.rayon),grossissement*(bul.xc+bul.rayon),grossissement*(bul.yc+bul.rayon), outline="black", fill=txtCouleur, width=2)
            bul.bouge()
    # détection des collisions et des rebonds éventuels
    detect_collision(Mousse)
    detect_bord_rebond(Mousse)
    # création aléatoire d'une bulle 
    alea = choice([True, False, False, False]) # une chance sur quatre
    if alea :
        newBulle = Cbulle()
        placeBulle(newBulle)
    # et on bouge
    can.after(100*speed_animation,animation)


def depart() :
    can.delete("all")
    for bul in Mousse :
        if bul is not None :
            txtCouleur = couleurBulle(bul.couleur)
            can.create_oval(grossissement*(bul.xc-bul.rayon),grossissement*(bul.yc-bul.rayon),grossissement*(bul.xc+bul.rayon),grossissement*(bul.yc+bul.rayon), outline="black", fill=txtCouleur, width=2)

    
# boutons
bottom_frame = Frame(fenetre, width=largeur, height=50,  bg='white')
bottom_frame.pack(side='bottom', fill='both', padx=10, pady=10, expand=True)
Button(bottom_frame, text = 'Lancer', command = animation).pack(side='left',padx = 5,pady = 5)

# corps du programme

depart()
fenetre.mainloop()