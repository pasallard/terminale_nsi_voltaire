from random import randint
from math import *

class Cbulle:
    def __init__(self):
        self.xc = randint(0, 100)
        self.yc = randint(0, 100)
        self.rayon = randint(1, 10) # mini = 1
        self.dirx = float(randint(-1, 1)) # dirx et diry valent
        self.diry = float(randint(-1, 1)) # -1.0 ou 0.0. ou 1.0
        self.couleur = randint(1,65535)

    def bouge(self):
    # déplace la bulle
        self.xc = self.xc + self.dirx
        self.yc = self.yc + self.diry
 
# création d'un jeu à 6 bulles        
Mousse = [Cbulle(), Cbulle(),Cbulle(), Cbulle(), Cbulle(), Cbulle()] 

# question 1-a
def donnePremierIndiceLibre(Mousse):
    """
    Mousse est une liste.
    La fonction doit renvoyer l’indice du premier emplacement libre (contenant None) dans la liste Mousse
    ou renvoyer 6 en l’absence d’un emplacement libre dans Mousse.
    """
    i = 0
    while ... and Mousse[i] != None :
        ...
    return i

# question 1-b
def placeBulle(B) :
    # à compléter

# code fourni 
def distanceEntreBulles(B1,B2) :
    """Renvoie la distance entre les centres des deux bulles"""
    d_carré = (B2.xc - B1.xc)**2 + (B2.yc - B1.yc)**2
    return sqrt(d_carré)

# question 2
def bullesEnContact(B1,B2) :
    # à compléter

# question 3
def collision(indPetite, indGrosse, Mousse) :
    """
    Absorption de la plus petite bulle d’indice indPetite
    par la plus grosse bulle d’indice indGrosse. Aucun test
    n’est réalisé sur les positions.
    """
    # calcul du nouveau rayon de la grosse bulle
    surfPetite = pi*Mousse[indPetite].rayon**2
    surfGrosse = pi*Mousse[indGrosse].rayon**2
    surfGrosseApresCollision = ... # compléter ici
    rayonGrosseApresCollision = sqrt(surfGrosseApresCollision/pi)
    # et on modifie le rayon (oubli de l'énoncé !)
    Mousse[indGrosse].rayon = rayonGrosseApresCollision
    #réduction de 50% de la vitesse de la grosse bulle
    Mousse[indGrosse].dirx = # compléter ici
    Mousse[indGrosse].diry = # compléter ici
    #suppression de la petite bulle dans Mousse
    # compléter ici


