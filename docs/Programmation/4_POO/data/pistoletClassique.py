def tirerUneBalle(nombreBalles):
    ballesApresTir = nombreBalles - 1
    print("Je tire : il y a ", ballesApresTir, " balles dans le pistolet")
    return ballesApresTir

def recharger():
    print("Je recharge : il y a 6 balles dans le pistolet")
    return 6

# Corps du programme
nbBallesPistolet = 0 #initialisation
nbBallesPistolet = recharger()
for tir in range(3) :
    nbBallesPistolet = tirerUneBalle(nbBallesPistolet)   