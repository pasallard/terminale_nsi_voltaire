def coloration(G) :
    """Renvoie une coloration du graphe G"""
    # version séquentielle, limitée à 6 couleurs

    couleur = ['Rouge', 'Bleu', 'Vert', 'Jaune', 'Noir', 'Blanc']
    coloration_sommets = {s_i: None for s_i in G}
    for s_i in G:
        couleurs_voisins_s_i = [coloration_sommets[s_j] for s_j in voisins(G, s_i)]
        k = 0
        while couleur[k] in couleurs_voisins_s_i:
            k = k + 1
        coloration_sommets[s_i] = couleur[k]
    return coloration_sommets