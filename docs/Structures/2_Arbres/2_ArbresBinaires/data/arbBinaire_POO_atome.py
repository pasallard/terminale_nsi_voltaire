#--- HDR ---#
class Noeud:

    def __init__(self,val = None, sag = None, sad = None) :
        self.valeur = val
        self.left = sag
        self.right = sad

P = Noeud("Proton") 
N = Noeud("Neutron")
E = Noeud("Electron" )
Y = Noeud("Noyau", P, N)
monArbreMystere = Noeud("Atome", E, Y)

#--- HDR ---#
print("Merci d'avoir executé le code : à vous de découvrir l'arbre binaire monArbreMystere")
