#--- HDR ---#
def valeur(noeud) :
    return noeud[0]

def sous_arbre_gauche(noeud) :
    return noeud[1]

def sous_arbre_droit(noeud) :
    return noeud[2]

def est_vide(noeud) :
    return len(noeud) == 0

Huit1= ("France", (), () )
Huit2 = ("Allemagne", (), () )
Huit3 = ("Suède", (), () )
Huit4 = ("Egypte", (), () )
Huit5 = ("Norvège", (), () )
Huit6 = ("Espagne", (), () )
Huit7 = ("Danemark", (), () )
Huit8 = ("Hongrie", (), () )
Quart1= ("France", Huit1, Huit2 )
Quart2 = ("Suède", Huit3, Huit4)
Quart3 = ("Espagne", Huit5, Huit6)
Quart4 = ("Danemark", Huit7, Huit8)
Demi1 = ("France", Quart1, Quart2 )
Demi2 = ("Danemark", Quart3, Quart4)
monArbre = ("Danemark", Demi1, Demi2 )
#--- HDR ---#
print("Merci d'avoir executé le code : à vous de découvrir l'arbre binaire monArbre")