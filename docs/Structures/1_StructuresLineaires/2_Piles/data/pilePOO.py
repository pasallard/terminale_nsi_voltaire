class Pile :
    def __init__(self) :
        self.contenu = [] # création d'une pile vide

    def empiler(self, donnée) :
        self.contenu.append(donnée)  # ajouter en tête de la pile = à la fin du tableau
    
    def depiler(self) :
        aEnlever= self.contenu.pop( len(self.contenu) - 1) #  la donnée à enlever est positionnée à la fin du tableau
        return aEnlever

    def est_vide(self) :
        return len(self.contenu) == 0

#     def __str__(self) : # pour pouvoir étendre la fonction print
#         message = "Voici la pile : " + str(self.contenu)
#         return message