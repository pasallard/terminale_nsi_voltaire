#--- HDR ---#
def creer_pile_vide()  :
    return []

def empiler(p, donnée) :
    p.append(donnée) # on ajoute la donnée à la fin du tableau
    return p # en fait, c'est superflu : en Python, la liste "pile" est modifiée en place 

def depiler(p) :
    aEnlever = p.pop( len(p) - 1 ) # la donnée à enlever est positionnée à la fin du tableau
    return aEnlever

def est_vide(p)  :
    return len(p) == 0 # renvoie le booléen True si la longueur est nulle et False sinon
#--- HDR ---#
# Corps du programme
pilePanoramix = creer_pile_vide() 
empiler(pilePanoramix,"Astérix")
empiler(pilePanoramix,"Cétautomatix")
empiler(pilePanoramix,"Ordralfabétix")
empiler(pilePanoramix,"Bonemine")
empiler(pilePanoramix,"Abraracourcix")
print("J'ai beaucoup de messages à lire ! ")
while not est_vide(pilePanoramix)  : 
    lecture = depiler(pilePanoramix)
    print("Je lis le message de", lecture)
print("J'ai lu tous les messages !")