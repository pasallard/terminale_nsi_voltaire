# Dictionnaires en Python :scroll:

## ^^1. Révisions^^

!!! tip "Révisions"
    On rappelle, ci-dessous, quelques commandes sur l'utilisation d'un dictionnaire en Python :

    * `dico = {}` ou encore `dico = dict()` crée un dictionnaire vide appelé `dico`
    * `dico[cle] = contenu` met la valeur `contenu` pour la clef `cle` dans le dictionnaire `dico` ; si la clef `cle` n'existe pas encore, elle est automatiquement créée
    * `dico[cle]` renvoie la valeur associée à la clef `cle` dans le dictionnaire `dico`
    * `cle in dico` (ou encore `cle in dico.keys()`)  renvoie un booléen indiquant si la clef `cle` est présente dans le dictionnaire `dico`
    * `for cle in dico:` (ou encore `for cle in dico.keys():`) permet d'itérer sur les clefs d'un dictionnaire.
    * `len(dico)` renvoie le nombre de clefs d'un dictionnaire.
    
    Pour plus de détails, revoir le [cours de Première sur les dictionnaires.](https://pasallard.gitlab.io/premiere_nsi_voltaire/TypesConstruits/pageDeGarde/)


!!! info "Supprimer une donnée d'un dictionnaire"
    Si on veut supprimer une donnée (à la fois la clef et sa valeur) d'un dictionnaire, on utilise le mot-clé `del` ( :flag_gb: :flag_us: delete) comme ceci :

    {{ IDE("data/revisions_del")}}



## ^^2. Concept de tableau associatif, implémentation par un dictionnaire^^

Un dictionnaire en Python (type `dict`) correspond à ce qui, de manière général en informatique, est appelé un **tableau associatif**.

Par exemple, dans un jeu de cartes, chaque "couleur" dispose un symbole. On peut donc concevoir un **tableau qui associe**, pour chaque couleur, le mot à son symbole :

<figure markdown>

| mot | symbole |
| :---: | :---: | 
| cœur | ♥ |
| carreau | ♦ | 
| pique | ♠ | 
| trèfle | ♣ | 
</figure>

Ce **concept de tableau associatif** (cette structure de données abstraite) est directement implémenté en Python par un dictionnaire :

```python
couleur = {"coeur" : "♥", "carreau" : "♦", "pique" : "♠", "trèfle" : "♣"} # on utilise ici des accolades
```

![](data/schema_dico_carte.png){: .center width 60%}

Avec ce dictionnaire, si l'on veut obtenir le symbole du cœur, il suffit de saisir 
```python
couleur["coeur"] # on utilise ici des crochets
```

Vous pouvez tester par vous-même :
{{ terminal() }}

!!! faq "Exercice"
    === "Question"
        Dans le dictionnaire `couleur` ci-dessus, la chaine de caractères `"carreau"` est-elle une clef ou une valeur ?
    === "Solution"
        La chaine de caractères `"carreau"` est une clef, et sa valeur est la chaine de caractères `"♦"` (réduite à un seul caractère Unicode).

## ^^3. Quelques explications sur le fonctionnement d'un dictionnaire^^

On pourrait penser que, quand un programme Python doit créer et manipuler un dictionnaire, il construit "en secret" un tableau à deux dimensions,   du style `tab_couleur = [["coeur","♥"], ["carreau","♦"], ["pique","♠"], ["trèfle","♣"]]`.

Mais ce n'est pas le cas car ce ne serait pas assez efficace pour accéder aux valeurs du dictionnaire. Par exemple, avec la clef `"trèfle"`, il faudrait parcourir toutes les cases du tableau `tab_couleur` jusqu'à ce qu'on y trouve cette clef et ensuite retourner la valeur qui lui est associée, à savoir le symbole `"♣"`. Le temps d'accès à une valeur du dictionnaire à partir de sa clef serait donc proportionnel à la taille du dictionnaire : la structure de dictionnaire permet de faire mieux que ça !

Lors de la création d'un dictionnaire, Python stocke les données (les clefs et leurs valeurs associées) dans un tableau à une dimension, à des ^^numéros de cases calculés à partir de la clef^^. Pour effectuer la conversion d'une clef (par exemple : les chaines de caractères "coeur", "carreau", etc .) en numéro de case, Python utilise une ***fonction de hachage***.

{{ IDE('data/script_hachage') }}

Grâce à cette technique, quand on veut accéder à la valeur correspondant à la clef `"trèfle"`, 

* Python calcule d'abord le numéro de la case du tableau où il a stocké cette donnée 
* puis il peut aller directement à la bonne case, sans passer en revue toutes les autres cases du tableau.

Ainsi, le temps d'accès à une valeur d'un dictionnaire n'est pas proportionnel à la taille du dictionnaire : c'est un **temps constant**.




## ^^Exercices et TP sur les dictionnaires^^

* Télécharger le [carnet Jupyter](TP_Exos_Dictionnaires.ipynb), à ouvrir si besoin sur [Basthon](https://notebook.basthon.fr/){target="_blank"}.

* Exercice type Bac n°1 : [énoncé](Dictionnaires_Exo.pdf).

* Exercice type Bac n°2 :  [énoncé](Dictionnaires_Tuples_CentresEtrangers_2022.pdf) ; vous pouvez tester vos réponses en ouvrant dans Thonny ce [fichier Python](Dictionnaires_Tuples_Dates.py).











