---
title: Dictionnaire modélisant le contenu d'un répertoire
author: Franck Chambon, modifié par Pierre-Alain
---
# Exercice type Bac

> D'après 2022, Centres étrangers, J1, Ex. 3

!!! danger ""
    Ne vous précipitez pas sur les solutions : il faut d'abord chercher sur le papier, puis tester les idées du code et chercher à se corriger soi-même avant d'ouvrir les onglets "Solutions".

Afin d'organiser les répertoires et les fichiers sur un disque dur, une structure arborescente est utilisée. Les fichiers sont dans des répertoires qui sont eux-mêmes dans d'autres répertoires, etc.

Dans une arborescence, chaque répertoire peut contenir des fichiers et des sous-répertoires, qui sont identifiés par leur nom. Le contenu d'un répertoire est modélisé par la structure de données **dictionnaire**. Les clefs de ce dictionnaire sont des chaines de caractères donnant le nom des fichiers et des sous-répertoires.


!!! example "Exemple illustré"
    Le répertoire appelé `Téléchargements` contient deux fichiers `rapport.pdf` et `jingle.mp3` ainsi qu'un répertoire `Images` contenant simplement le fichier `logo.png`.

    Il est représenté ci-dessous.

    ```mermaid
    %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
    flowchart TB
        n0[[Téléchargement]] --> n1[[Images]]
        n1 --> n4(logo.png)
        n0 --> n2(rapport.pdf)
        n0 --> n3(jingle.mp3)
    ```

    Ce répertoire `Téléchargements` est modélisé en Python par le dictionnaire suivant :

    ```python 
    {"Images": {"logo.png": 36}, "rapport.pdf": 450, "jingle.mp3": 4800}
    ```

    Les valeurs numériques correspondent à la taille des fichiers et sont exprimées en ko (kilo-octets).

    Par exemple, `"logo.png": 36` signifie que le fichier `logo.png` occupe un espace mémoire de 36 ko sur le disque dur.



Pour cet exercice, l'**adresse** d'un fichier ou d'un répertoire correspond au nom de tous les répertoires à parcourir depuis la racine afin d'accéder au fichier ou au répertoire. Cette adresse est modélisée en Python par la ***liste*** des noms de répertoire à parcourir pour y accéder.

Exemple : L'adresse du répertoire : `/home/pierre/Documents/` est modélisée par la liste `["home", "pierre", "Documents"]`.

**1.** Dessiner l'arborescence donné par le dictionnaire `docs` suivant, qui correspond au répertoire `Documents`.

{{ IDE("data/22G11J1ex3q1")}}

??? danger "Réponse"

    ```mermaid
    %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
    flowchart TB
        doc[[Documents]] --> adm[[Administratif]]
        adm --> certif(certificat_JDC.pdf)
        adm --> attest(attestation_recensement.pdf)

        doc --> cours[[Cours]]
        cours --> nsi[[NSI]]
        nsi --> tp(TP.html)
        nsi --> dm(dm.odt)

        cours --> philo[[Philo]]
        philo --> tlp(Tractatus_logico-philosophicus.epub)

        doc --> lst(liste_de_courses.txt)
    ```

**2.** On donne la fonction _incomplète_ `parcourt` suivante qui prend en paramètres un répertoire `racine` et une liste `adr` représentant une adresse, et qui renvoie le contenu du répertoire cible correspondant à l'adresse.

Exemple : Si la variable `docs` contient le dictionnaire de l'exemple de la question 1 alors `parcourt(docs, ["Cours", "Philo"])` renvoie le dictionnaire ` {"Tractatus_logico-philosophicus.epub": 2600}`.

**2.a.** Compléter la ligne 4 :

{{ IDEv("data/22G11J1ex3q2")}}

??? danger "Réponse"

    ```python
    def parcourt(racine, adr):
        repertoire = racine
        for nom_repertoire in adr:
            repertoire = repertoire[nom_repertoire]
        return repertoire
    ```

**2.b.** Soit la fonction suivante :

```python
def affiche(racine, adr, nom_fichier):
    repertoire = parcourt(racine, adr)
    print(repertoire[nom_fichier])
```

Qu'affiche l'instruction `affiche(docs, ["Cours", "NSI"], "TP.html")` sachant que la variable `docs` contient le dictionnaire de la question 1 ?

??? danger "Réponse"

    - La première instruction fait que `repertoire` correspond au dictionnaire `"NSI"` qui vaut `{"TP.html": 60, "dm.odt": 345}`.
    - La seconde affiche la valeur associée à la clef `"TP.html"` de ce dictionnaire, c'est-à-dire le poids en ko de ce fichier.

    L'affichage est donc

    ```
    >>> affiche(docs, ["Cours", "NSI"], "TP.html")
    60
    ```

**3.a.** La fonction `ajoute_fichier` suivante, de paramètres `racine`, `adr`, `nom_fichier` et `taille`, ajoute au dictionnaire `racine`, à l'adresse `adr`, la clef `nom_fichier` associée à la valeur `taille`.

Une ligne de la fonction donnée ci-dessous contient une erreur. Laquelle ? Proposer une correction.

{{ IDE("data/22G11J1ex3q3a")}}

??? danger "Réponse"
    L'erreur est à la ligne 3 et elle doit être corrigée ainsi :

    ```python
    def ajoute_fichier(racine, adr, nom_fichier, taille):
        repertoire = parcourt(racine, adr)
        repertoire[nom_fichier] = taille
        return None
    ```

**3.b.** Écrire alors l'instruction pour ajouter le fichier `"monProg.py"` de taille 33 ko dans le répertoire `/Documents/Cours/NSI` (avec toujours la situation de la question 1).

{{ terminal()}}

??? danger "Réponse"
    `ajoute_fichier(docs, ["Cours", "NSI"], "monProg.py", 33)`

**3.c.** Écrire une fonction `ajoute_repertoire` de paramètres `racine`, `adr` et `nom_repertoire` qui crée un dictionnaire représentant un répertoire vide appelé `nom_repertoire` dans le dictionnaire `racine` à l'adresse `adr`. _Écrire cette fonction plus haut, à la suite de la fonction `ajoute_fichier`_.


??? danger "Réponse"

    ```python
    def ajoute_repertoire(racine, adr, nom_repertoire):
        repertoire = parcourt(racine, adr)
        repertoire[nom_repertoire] = dict()
    ```

**3.d.** Écrire alors l'instruction pour ajouter le répertoire `"HistoireGeo"` comme sous-répertoire de `/Documents/Cours` (avec toujours la situation de la question 1). Ce répertoire `"HistoireGeo"` doit donc être au même niveau de `"NSI"` et `"Philo"`.

{{ terminal()}}

??? danger "Réponse"
    `ajoute_repertoire(docs, ["Cours"], "HistoireGeo")`


**4.**  Écrire une fonction `taille` qui prend en paramètre un dictionnaire `racine` modélisant un répertoire et qui renvoie le total d'espace mémoire occupé par les fichiers contenus dans ce répertoire. On suppose ici que le répertoire ne contient que des fichiers et ***aucun sous-répertoire***, comme c'est le cas par exemple du répertoire représenté par la variable `monRep` ci-dessous :

{{ IDE("data/22G11J1ex3q4")}}

??? danger "Réponse"

    ```python
    def taille(racine) :
        total = 0
        for nom_fichier in racine.keys() :
            total = total + racine[nom_fichier]
        return total
    ```
