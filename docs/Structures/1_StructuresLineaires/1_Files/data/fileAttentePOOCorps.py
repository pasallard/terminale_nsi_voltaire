#--- HDR ---#
class File :
    def __init__(self) :
        self.contenu = [] # création d'une file vide

    def enfiler(self, donnée) :
        self.contenu.append(donnée) # ajouter en queue de la file
    
    def defiler(self) :
        tête_de_file = self.contenu.pop(0) # enlever la première valeur de la file
        return tête_de_file

    def est_vide(self) :
        return len(self.contenu) == 0 # renvoie le booléen True si la longueur est nulle et False sinon

#     def __str__(self) : # pour pouvoir étendre la fonction print
#         message = "Voici la file d'attente : " + str(self.file)
#         return message
#--- HDR ---#
# Corps du programme
fileChaudron = File() # création d'une instance de la classe FileAttente
fileChaudron.enfiler("Astérix")
fileChaudron.enfiler("Cétautomatix")
fileChaudron.enfiler("Ordralfabétix")
fileChaudron.enfiler("Bonemine")
fileChaudron.enfiler("Abraracourcix")
while not fileChaudron.est_vide()  : 
    servi = fileChaudron.defiler()
    print(servi, " est maintenant servi")
print("Tout le monde a été servi !")