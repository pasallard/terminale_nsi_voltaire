#--- HDR ---#
def creer_file_vide()  :
    return []

def enfiler(f, donnée)  :
    f.append(donnée) # ajouter en queue de la file
    return f # en fait, c'est superflu : en Python, la liste "file" est modifiée en place

def defiler(f)  :
    tête_de_file = f.pop(0) # enlever la première valeur de la file
    return tête_de_file

def est_vide(f)  :
    return len(f) == 0 # renvoie le booléen True si la longueur est nulle et False sinon
#--- HDR ---#
# Corps du programme
fileChaudron = creer_file_vide()
print("Qui veut de la potion magique ?")
enfiler(fileChaudron,"Astérix")
enfiler(fileChaudron,"Cétautomatix")
enfiler(fileChaudron,"Ordralfabétix")
enfiler(fileChaudron,"Bonemine")
enfiler(fileChaudron,"Abraracourcix")
while not est_vide(fileChaudron)  : 
    servi = defiler(fileChaudron)
    print(servi, " a reçu sa gorgée de potion")
print("Tout le monde a été servi !")