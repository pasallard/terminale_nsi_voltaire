# Cours de Terminale NSI

## ^^Contenu du cours^^ :cinema:

Ce cours est destiné à vous préparer dans de bonnes conditions aux épreuves pratique et écrite du baccalauréat.

Il est volontairement limité au strict nécessaire. Il doit susciter un certain nombre de questions de votre part : votre professeur est là pour vous guider et vous apporter des éléments de réponse.

Pour des approfondissements, voici quelques sites d'autres enseignants de NSI : 

* [Gilles Lassus](https://glassus.github.io/terminale_nsi){target="_blank"}  
* [Mireille Coilhac](https://mcoilhac.forge.apps.education.fr/term/){target="_blank"}
* [Rodrigo Schwencke](https://eskool.gitlab.io/tnsi/){target="_blank"} 

Et pour réviser le programme de Première, c'est [ici](https://pasallard.gitlab.io/premiere_nsi_voltaire/){target="_blank"}.


## ^^Sites d'entrainement aux épreuves du Bac^^ :factory:

Liens vers des sites, à utiliser sans modération :

* [Exercices de l'Épreuve Pratique 2024](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2024/){target="_blank"}.

* Exercices plus diversifiés, plus intéressants (moins de bachotage) : 

    - [Banque d'exercices NSI-Pratique](https://pratique.forge.apps.education.fr/){target="_blank"}
    - [CodEx](https://codex.forge.apps.education.fr/){target="_blank"}

* Annales des sujets de Bac, avec des corrigés : [en version pdf](https://pixees.fr/informatiquelycee/term/index.html#suj_bac){target="_blank"}

## ^^Quelques ressources pour le Grand Oral^^ 👩‍🎤

* [Présentation général et conseils](https://nsimeyroneinc.github.io/NSITerm/oral/oral/) 
* Des idées de sujets [ ici](http://www.maths-info-lycee.fr/grantoral.html) et aussi [ici](http://wiki.goupill.fr/doku.php?id=nsi:terminales:grand_oral:presentations_orales).


## ^^Outils et mode d'emploi^^ :trident:

!!! info "Outils indispensables"

    * Pour la programmation en Python, vous avez besoin d'un "éditeur Python" (_IDE_) : [Thonny](https://thonny.org/){target="_blank"} est l'outil le plus simple, mais vous pouvez aussi utiliser [Edupython](https://edupython.tuxfamily.org/#téléchargement){target="_blank"}, Spyder, VSCode/VSCodium, etc.

    * La plupart des exercices sont présentés dans des "carnets Jupyter" (_Jupyter Notebook_), comme [celui-ci](Programmation/IntroJupyter.ipynb) par exemple : 

        - il faut les télécharger en faisant un ***clic droit*** sur le lien, puis ***Enregistrer le lien sous...***
        - vous pouvez les lire en les ouvrant sur le site [Basthon](https://notebook.basthon.fr/){target="_blank"} ou bien avec une application Jupyter installée sur votre ordinateur.

!!! tip "Conseil"
    Edupyter, disponible [ici](https://www.edupyter.net/){target="_blank"}, donne accès à la fois à Thonny et à Jupyter :+1:. 

    Il s'installe sur un PC Windows **sans les droits administrateurs**.  Il se lance en cliquant sur l'icône qui apparaît à côté de l'horloge (voir [schéma](https://raw.githubusercontent.com/edupyter/documentation/main/edupyter-help.png){target="_blank"}).







