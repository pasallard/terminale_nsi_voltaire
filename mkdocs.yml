site_name: "NSI Voltaire (Terminale)"
site_author: Pierre-Alain SALLARD
site_url: !ENV [CI_PAGES_URL, "http://127.0.0.1:8000/"]
site_description: Pour NSI en Terminale



copyright: |
    Pierre-Alain SALLARD
    <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>.

docs_dir: docs

nav:
  - "🐝 Accueil ": index.md
  # - ... | regex=^(?:(?!_REM.md).)*$
  # - "👻 Révisions Bac" : Revisions/Revisions.md
  - "🎮 Programmation" : 
    - "🎮" : Programmation/pageDeGarde.md
    - "🐍 Révisions et compléments" : Programmation/1_Revisions/Prog_RevisionsComplements.md
    - "🌃 Rappels sur le codage des nombres entiers" : Programmation/5_CodageEntiers/EntiersRelatifs_Cours.md
    - "🧩 Modularité" :
      - "Modularité" : Programmation/3_Modularite/Prog_Modularite.md
      - "TP Traitement d'images" : Programmation/3_Modularite/TP_Traitement_Image/Modularite_TP_Traitement_Image.md
    - "🪆 Récursivité" : Programmation/2_Recursivite/Prog_Recursivite.md
    - "🔫 Programmation Orientée Objet" : 
      - "Programmation Orientée Objet" : Programmation/4_POO/Prog_POO.md
      - "Exo Bac POO + Pile" : Programmation/4_POO/Prog_POO_22-PO1-ex4.md
  - "🏯 Structures de données" :
    - "🏯" : Structures/pageDeGarde.md
    - "📜 Dictionnaires" : 
      - "Cours" : Structures/1_StructuresLineaires/4_Dictionnaires/Structures_Dictionnaires.md
      - "Exercice type Bac" : Structures/1_StructuresLineaires/4_Dictionnaires/SujetBac-22-G11-J1-ex3.md
    - "📚 Structures Linéaires" :
      - "Files" : Structures/1_StructuresLineaires/1_Files/Structures_Files.md
      - "Piles" : Structures/1_StructuresLineaires/2_Piles/Structures_Piles.md
      - "Listes chainées" : Structures/1_StructuresLineaires/3_ListesChainees/Structures_ListesChainees.md
    - "🌴 Arbres" :
      - "Généralités sur les arbres" : Structures/2_Arbres/1_Intro/Structures_Arbres_Intro.md
      - "Arbres binaires" : Structures/2_Arbres/2_ArbresBinaires/Structures_ArbresBinaires.md
      - "ABR" : Structures/2_Arbres/3_ArbresBinairesRecherche/Structures_ABR.md
    - "🕸️ Graphes" : 
      - "Cours et exercices" : Structures/03_Graphes/IntroGraphes.md
      - "TD Coloration d'un graphe" : Structures/03_Graphes/TD_ColorationGraphe.md
  - "🎢 Algorithmique" :
    - "🎢" : Algorithmique/pageDeGarde.md
    - "Révisions et compléments du cours de Première" :
      - "🎭 Algorithmes de tri" : Algorithmique/1_ComplementsPremiere/Algo_RappelTris.md
      - "🐗 Algorithmes gloutons" : Algorithmique/1_ComplementsPremiere/Algo_Revisions_Gloutons.md
      - "💔 Algorithme de recherche par dichotomie" : Algorithmique/1_ComplementsPremiere/Algo_RappelDicho.md
    - "Algorithmes & Arbres" :
      - "🐒 Parcours d'un arbre" : Algorithmique/2_Arbres/Algo_Arbres_Parcours.md
      - "🎯 ABR : insertion et recherche" : Algorithmique/2_Arbres/Algo_ABR.md
    - "Algorithmes & Graphes" :
      - "🎡 Parcours d'un graphe" : Algorithmique/3_Graphes/Algo_Parcours_Graphes.md
    - "Autres algorithmes" : 
      - "💃 Programmation dynamique" : Algorithmique/6_ProgrammationDynamique/ProgDynamique.md
      - "🌺 PopArt et Plus Proches Voisins" : Algorithmique/4_KNN/KNN_PopArt.md
  - "🐧 Architecture matérielle" :
    - "🐧" : ArchitectureMaterielle/pageDeGarde.md
    - "🐧 GNU/Linux" : ArchitectureMaterielle/0_Linux/Archi_GNU_Linux.md
    - "🌀 Gestion des processus" : ArchitectureMaterielle/1_GestionProcessus/Archi_Gestion_Processus.md
    - "Réseaux" :
      - "📬 Adressage dans un réseau" : ArchitectureMaterielle/2_Reseaux/Archi_Reseaux_Adressage.md
      - "🚎 Routage" : ArchitectureMaterielle/3_Routage/Archi_Reseaux_Routage.md
      - "Exercice type Bac" : ArchitectureMaterielle/3_Routage/22-ME2-ex3.md
    - "📱 Systèmes sur puce" : ArchitectureMaterielle/4_SoC/Archi_SoC.md
    - "🕵🏽‍♀️ Cryptographie" : 
      - "Chiffrements symétrique et asymétrique" : ArchitectureMaterielle/5_Crypto/Crypto.md
      - "Stéganographie" : ArchitectureMaterielle/5_Crypto/Stegano.md
  - "🔭 Bases de données" :
    - "🔭" : BasesDeDonnees/pageDeGarde.md
    - "Introduction au langage SQL et à la gestion des bases de données" :  BasesDeDonnees/BdD_Intro.md
    - "Exercices et Travaux Pratiques en SQL" : BasesDeDonnees/TP_SQL.md
    - "Croisement de données et jointures de tables" : BasesDeDonnees/BdD_Jointure.md

  - "🚀 Études PostBac" : Misc/OrientationInformatiqueNumerique.md


theme:
    favicon: assets/images/favicon.png
    icon:
      logo: material/lightbulb-group
    name: pyodide-mkdocs-theme

    features:
        #- navigation.instant
        #- navigation.tabs
        - navigation.top
        - toc.integrate
        - header.autohide
        - content.code.copy   ## Ajout comparaison Angélique
        - content.code.annotate   # Pour les annotations de code deroulantes avec +


markdown_extensions:
    - md_in_html
    - meta
    - abbr

    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Pseudo-code"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 

    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji: # Émojis  :boom:
        emoji_index: !!python/name:material.extensions.emoji.twemoji
        emoji_generator: !!python/name:material.extensions.emoji.to_svg

    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format

    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 4


plugins:

    - awesome-pages:
        collapse_single_pages: true

    - search
    - tags:
        tags_file: tags.md
    - pyodide_macros:
      # Vous pouvez ajouter ici tout réglage que vous auriez ajouté concernant les macros:
        on_error_fail: true     # Il est conseillé d'ajouter celui-ci si vous ne l'utilisez pas.
        build:
          tab_to_spaces: 4


# En remplacement de mkdocs-exclude. Tous les fichiers correspondant aux patterns indiqués seront
# exclu du site final et donc également de l'indexation de la recherche.
# Nota: ne pas mettre de commentaires dans ces lignes !
exclude_docs: |
    **/*_REM.md
  


extra_css:
  - xtra/stylesheets/ajustements.css  # ajustements
